from operator import attrgetter, itemgetter
from datetime import datetime
import requests
from bs4 import BeautifulSoup
from pymongo import MongoClient
import configparser

config = configparser.ConfigParser()
config.optionxform = str
host = config['DEFAULT']['mongodb_host']
port = config.get('DEFAULT', 'mongodb_port')
client = MongoClient(host, port)
db = client['stock']
agents = db['agents']
collectTWSE = db['twse_list']

headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36"}

twse = collectTWSE.find()
row_count = twse.count()

for index, item in enumerate(twse):
    stock_no = item['stock_id']
    stock_name = item['stock_name']
    print("stock no {0}-{1}".format(stock_no, stock_name))
    items = agents.find_one({"stockNo": stock_no})

    if items is not None:
        details = items['details']
        details.sort(key=itemgetter('date'), reverse=True)

        if len(details) == 0:
            continue

        db_date = details[0]['date'].strftime("%Y-%m-%d 00:00:00")
        today = datetime.now().strftime("%Y-%m-%d 00:00:00")

        if len(details) > 0:
            if db_date == today:
                print("Data exist")
                continue

    url = "http://www.wantgoo.com/stock/astock/agentstat2?stockno={0}".format(stock_no)
    res = requests.post(url, headers=headers)
    soup = BeautifulSoup(res.text, 'html.parser')
    table = soup.find('table', attrs={'id': 'listResult'})

    if table is None:
        continue
    table_body = table.find('tbody')
    rows = table_body.find_all('tr')
    data = []

    json_set_array = []

    for row in rows:
        cols = row.find_all('td')
        cols = [ele.text.strip() for ele in cols]

        buy_sell = ""
        store_count = ""
        five_centralized = ""
        twenty_centralized = ""

        if cols[0] == '目前權證,興櫃暫不提供資料.':
            continue

        try:
            date = cols[0]
            if date == '':
                date = datetime.today()
        except IndexError:
            date = ""

        try:
            buy_sell = cols[1].replace(',', '')
            if buy_sell == '':
                buy_sell = "0"
        except IndexError:
            buy_sell = "0"

        try:
            store_count = cols[2].replace(',', '')
            if store_count == '':
                store_count = "0"
        except IndexError:
            store_count = "0"

        try:
            five_centralized = cols[3].replace('%', '')
            if five_centralized == '':
                five_centralized = "0"
        except IndexError:
            five_centralized = "0"

        try:
            twenty_centralized = cols[4].replace('%', '')
            if twenty_centralized == '':
                twenty_centralized = "0"
        except IndexError:
            twenty_centralized = "0"

        json_set = {'date': datetime.strptime(date, '%Y/%m/%d'),
                    'buySell': int(buy_sell),
                    'storeCount': int(store_count),
                    'fiveCentralized': float(five_centralized),
                    'twentyCentralized': float(twenty_centralized)
                    }
        json_set_array.append(json_set)

    if items is not None:
        agents.update({"stockNo": stock_no}, {'$push': {'details': json_set_array[0]}})
    else:
        stock_data = {
            'stockNo': stock_no,
            'stockName': stock_name,
            'details': json_set_array
        }
        agents.insert_one(stock_data)
