import talib


class GoldKDJ(object):
    def __init__(self, details, high, low, close):
        """
        KD 黃金交叉
        :param details: 
        :param high: 最高價
        :param low: 最低價
        :param close: 收盤價
        """
        self.details = details
        self.high = high
        self.low = low
        self.close = close

    def run(self):
        slowk, slowd = talib.STOCH(self.high, self.low, self.close, fastk_period=9, slowk_period=3, slowk_matype=0,
                                   slowd_period=3,
                                   slowd_matype=0)
        return slowk[-1] > slowk[-2] and slowk[-1] > slowd[-1] and slowd[-2] > slowk[-2]
