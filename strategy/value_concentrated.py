class ValueConcentrated(object):
    def __init__(self, details, concentrated=5, percentage=10, operation=5):
        """
        籌碼集中度
        5日集中度%=(買方前15名近5日總買超量-賣方前15名近5日總賣超量)/近5日總成交量 
        :param concentrated: 集中度天數 
        :param details: DataSet
        :param percentage: 集中度百分比
        :param operation: 預設大於或等於
        """
        self.details = details
        self.concentrated = concentrated
        self.operation = operation
        self.percentage = percentage

    def run(self):
        percentage = 0.0
        buy = 0.0
        val = 0.0
        for detail in self.details[-self.concentrated:]:
            b = float(detail['buySell'])
            d = float(detail['dealValue'])
            buy = buy + b
            val = val + d
        if val > 0:
            percentage = (buy / val) * 100

        if percentage > self.percentage:
            return True
        else:
            return False
