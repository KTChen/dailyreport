from enum import Enum


class Operation(Enum):
    eq = 1  # 等於
    neq = 2  # 不等於
    lt = 3  # 小於
    lte = 4  # 小於或等於
    gte = 5  # 大於或等於
    gt = 6  # 大於
