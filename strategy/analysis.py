#!/usr/bin/python
# -*-coding:utf-8 -*-
from pyalgotrade import strategy
from pyalgotrade.bar import BasicBar, Bars
from pyalgotrade.barfeed import Frequency, BaseBarFeed
from pyalgotrade.technical import ma, rsi, bollinger, stoch
from pymongo import MongoClient
from pyalgotrade.stratanalyzer.returns import Returns
from pyalgotrade.stratanalyzer import sharpe
from pyalgotrade.stratanalyzer import drawdown
from pyalgotrade.stratanalyzer import trades
from pyalgotrade import plotter
import pandas as pd

from customfeed import Feed

client = MongoClient('61.70.176.56', 27017)
db = client['stock']
collect = db['stock']
major = db['major']
obs_analysis_tabel = db['obs_analysis']
chip_db = db['chip']
real_time = db['real_time']
agents = db['agents']
daily_analysis_collect = db['daily_analysis']
daily_analysis_collect2 = db['daily_analysis2']
collectTWSE = db['twse_list']
collectOTC = db['otc_list']

item = collect.find({'stockNo': '3019'})
details = item[0]['details']
df = pd.DataFrame(list(details),
                  columns=['date', 'closePrice', 'highPrice', 'lowPrice', 'openPrice', 'dealValue', 'Adj Close'])
df["Adj Close"] = 0
df["buysell"] = 0
df = df.rename(
    columns={'closePrice': 'Close', 'date': 'Date', 'highPrice': 'High', 'lowPrice': 'Low', 'openPrice': 'Open',
             'dealValue': 'Volume'})

item2 = collect.find({'stockNo': '3443'})
details2 = item2[0]['details']
df2 = pd.DataFrame(list(details2),
                   columns=['date', 'closePrice', 'highPrice', 'lowPrice', 'openPrice', 'dealValue', 'Adj Close'])
df2["Adj Close"] = 0
df2["buysell"] = 0
df2 = df2.rename(
    columns={'closePrice': 'Close', 'date': 'Date', 'highPrice': 'High', 'lowPrice': 'Low', 'openPrice': 'Open',
             'dealValue': 'Volume'})


# df.to_csv('3019.csv')

# Example BarFeed for dataframes with data for a single instrument.
class DataFrameBarFeed(BaseBarFeed):
    """
    Expects a pandas dataframe in the following format:
    Open, High, Low, Close in that order as float64
    datetime64[ns] as dataframe index,
    check dataframe with df.dtypes.
    """

    def __init__(self, dataframe, instrument, frequency):
        super(DataFrameBarFeed, self).__init__(frequency)
        self.registerInstrument(instrument)
        # make a list of lists containing all information for fast iteration
        self.__df = dataframe.values.tolist()
        self.__instrument = instrument
        self.__next = 0
        self.__len = len(self.__df)

    def setUseAdjustedValues(self, useAdjusted):
        # does this have a function? I still have to use adjusted closes below twice
        return False

    def reset(self):
        super(DataFrameBarFeed, self).reset()
        self.__next = 0

    def peekDateTime(self):
        return self.getCurrentDateTime()

    def getCurrentDateTime(self):
        if not self.eof():
            rowkey = self.__df[self.__next][5]
            # rowkey does not need to call todatetime, it should already be in that format
        return rowkey

    def getDateTime(self):
        if not self.eof():
            rowkey = self.__df[self.__next][5]
            # rowkey does not need to call todatetime, it should already be in that format
        return rowkey

    def barsHaveAdjClose(self):
        return False

    def getNextBars(self):
        ret = None
        if not self.eof():
            # Convert the list of lists into a bar.BasicBar
            # iteration through list of lists is 4x faster then using a dataframe because
            # a lot of functions get called every iteration

            extra = {'buySell': self.__df[self.__next][7]}
            bar_dict = {
                self.__instrument: BasicBar(
                    self.__df[self.__next][0],
                    self.__df[self.__next][4],
                    self.__df[self.__next][2],
                    self.__df[self.__next][3],
                    self.__df[self.__next][1],
                    self.__df[self.__next][5],
                    0,
                    self.getFrequency(),
                    # is there another class I can use besides BasicBar that does
                    # not need an adjusted close(i.e. forex)?
                    # unused data will slow down the script
                    # dirty fix: use close as adjusted close
                    extra

                )
            }
            ret = Bars(bar_dict)
            self.__next += 1
        return ret

    def eof(self):
        # any particular reason to not get len() upon init?
        # would prob. be faster for multiple requests, about -7ms
        return self.__next >= self.__len

    def start(self):
        pass

    def stop(self):
        pass

    def join(self):
        pass


class MyStrategy(strategy.BacktestingStrategy):
    def __init__(self, feed, instrument, smaPeriod):
        strategy.BacktestingStrategy.__init__(self, feed, 100000)
        self.__position = None
        self.__tech = {}
        for i in instrument:
            self.__tech.update({i: [stoch.StochasticOscillator(feed[i], 9),
                                    rsi.RSI(feed[i].getCloseDataSeries(), 14),
                                    ma.SMA(feed[i].getPriceDataSeries(), smaPeriod),
                                    bollinger.BollingerBands(feed[i].getCloseDataSeries(), 20, 2.1)
                                    ]})

            self.__instrument = instrument

    def onEnterOk(self, position):
        execInfo = position.getEntryOrder().getExecutionInfo()
        f = position.getInstrument()

        self.info("buy : {0} - {1}".format(execInfo, f))

    def onEnterCanceled(self, position):
        self.__position = None

    def onExitOk(self, position):
        execInfo = position.getExitOrder().getExecutionInfo()
        self.info("sell : {0}".format(execInfo))
        self.__position = None

    def onExitCanceled(self, position):
        # If the exit was canceled, re-submit it.
        self.__position.exitMarket()

    def onBars(self, bars):
        """

        :param bars:
        :return:
        """

        for i in self.__instrument:

            bar = bars[i]
            columns = bar.getExtraColumns()

            bb = self.__tech[i][3]
            kd = self.__tech[i][0]
            if self.__position is None:
                if bars[i].getPrice() <= bb.getLowerBand()[-1]:
                    shares = int(self.getBroker().getCash() * 0.5 / bars[i].getPrice())
                    self.__position = self.enterLong(i, shares, True)
            elif not self.__position.exitActive() and kd[-1] > 90:
                self.__position.exitMarket()

    def getSMA(self):
        return self.__tech[self.__instrument['3443']]['sma']

    def getKD(self):
        i = self.__instrument[0]
        return self.__tech[i][0]


def run_strategy(smaPeriod):
    instrument = ["3443", "3019"]

    # feed1 = Feed()
    # feed1.addBarsFromCSV("3443", "3443.csv")
    # feed1.addBarsFromCSV("3019", "3019.csv")

    # feed = DataFrameBarFeed(feed1, instrument, Frequency.DAY)
    ff = Feed(Frequency.DAY)
    ff.setDateTimeFormat('%Y-%m-%d')
    ff.addBarsFromDataFrame('3019', df)
    ff.addBarsFromDataFrame('3443', df2)
    myStrategy = MyStrategy(ff, instrument, smaPeriod)

    retAnalyzer = Returns()
    myStrategy.attachAnalyzer(retAnalyzer)
    sharpeRatioAnalyzer = sharpe.SharpeRatio()
    myStrategy.attachAnalyzer(sharpeRatioAnalyzer)
    drawDownAnalyzer = drawdown.DrawDown()
    myStrategy.attachAnalyzer(drawDownAnalyzer)
    tradesAnalyzer = trades.Trades()
    myStrategy.attachAnalyzer(tradesAnalyzer)

    plt = plotter.StrategyPlotter(myStrategy, True, False, True)

    # plt.getOrCreateSubplot("KD").addDataSeries("K-D", myStrategy.getKD())

    # plt.getOrCreateSubplot("KD").addDataSeries("D", myStrategy.getKD().getD())

    myStrategy.run()
    print("投資組合最終價值 Final portfolio value: $%.2f" % myStrategy.getResult())
    print("累計報酬率 Cumulative returns: %.2f %%" % (retAnalyzer.getCumulativeReturns()[-1] * 100))
    print("夏普比率 Sharpe ratio: %.2f" % (sharpeRatioAnalyzer.getSharpeRatio(0.05)))
    print("最大交易回落 Max. drawdown: %.2f %%" % (drawDownAnalyzer.getMaxDrawDown() * 100))
    print("最長交易時間持續 Longest drawdown duration: %s" % (drawDownAnalyzer.getLongestDrawDownDuration()))
    print("--------------------------------------------")
    print("總共交易次數 Total trades: %d" % (tradesAnalyzer.getCount()))
    if tradesAnalyzer.getCount() > 0:
        profits = tradesAnalyzer.getAll()
        print("平均獲利 Avg. profit: $%2.f" % (profits.mean()))
        print("獲利標準差 Profits std. dev.: $%2.f" % (profits.std()))
        print("最大獲利 Max. profit: $%2.f" % (profits.max()))
        print("最小獲利 Min. profit: $%2.f" % (profits.min()))
        returns = tradesAnalyzer.getAllReturns()
        print("平均報酬率 Avg. return: %2.f %%" % (returns.mean() * 100))
        print("報酬率標準差 Returns std. dev.: %2.f %%" % (returns.std() * 100))
        print("最大報酬率 Max. return: %2.f %%" % (returns.max() * 100))
        print("最小報酬率 Min. return: %2.f %%" % (returns.min() * 100))

    print("--------------------------------------------")
    print("獲利交易 Profitable trades: %d" % (tradesAnalyzer.getProfitableCount()))
    if tradesAnalyzer.getProfitableCount() > 0:
        profits = tradesAnalyzer.getProfits()
        print("平均獲利 Avg. profit: $%2.f" % (profits.mean()))
        print("獲利標準差 Profits std. dev.: $%2.f" % (profits.std()))
        print("最大獲利 Max. profit: $%2.f" % (profits.max()))
        print("最小獲利 Min. profit: $%2.f" % (profits.min()))
        returns = tradesAnalyzer.getPositiveReturns()
        print("平均報酬率 Avg. return: %2.f %%" % (returns.mean() * 100))
        print("報酬率標準差 Returns std. dev.: %2.f %%" % (returns.std() * 100))
        print("最大報酬率 Max. return: %2.f %%" % (returns.max() * 100))
        print("最小報酬率 Min. return: %2.f %%" % (returns.min() * 100))

    print("--------------------------------------------")
    print("虧損交易 Unprofitable trades: %d" % (tradesAnalyzer.getUnprofitableCount()))
    if tradesAnalyzer.getUnprofitableCount() > 0:
        losses = tradesAnalyzer.getLosses()
        print("平均虧損 Avg. loss: $%2.f" % (losses.mean()))
        print("虧損標準差 Losses std. dev.: $%2.f" % (losses.std()))
        print("最大虧損 Max. loss: $%2.f" % (losses.min()))
        print("最小虧損 Min. loss: $%2.f" % (losses.max()))
        returns = tradesAnalyzer.getNegativeReturns()
        print("平均報酬率 Avg. return: %2.f %%" % (returns.mean() * 100))
        print("報酬率標準差 Returns std. dev.: %2.f %%" % (returns.std() * 100))
        print("最大報酬率 Max. return: %2.f %%" % (returns.max() * 100))
        print("最小報酬率 Min. return: %2.f %%" % (returns.min() * 100))

    plt.plot()


run_strategy(15)
