import talib


class BullMarket(object):
    def __init__(self, price, *timeperiods):
        """
        多頭排列
        :param price: 收盤價
        :param timeperiods: 平均移動線天數 
        """
        self.price = price
        self.timeperiods = []
        for item in timeperiods:
            self.timeperiods.append(item)

    def run(self):
        count = 0
        for time in self.timeperiods:
            avg = talib.SMA(self.price, timeperiod=time)
            if avg[-1] > avg[-2]:
                count += 1
        return count == len(self.timeperiods)
