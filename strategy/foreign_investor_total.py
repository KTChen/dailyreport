class GoldKDJ(object):
    def __init__(self, details, timeperiod=10, operation=5, total=0):
        """
        外資買賣超
        :param details: 
        :param timeperiod: 天數
        :param operation: 大於或小於 預設為大於等於
        :param total: 超過張數
        """
        self.details = details
        self.timeperiod = timeperiod
        self.operation = operation
        self.total = total

    def run(self):
        foreign_investor = 0.0
        for detail in self.details[-self.timeperiod:]:
            try:
                if 'foreign_investor_total' in detail:
                    foreign_investor = foreign_investor + float(detail['foreign_investor_total'])
            except Exception as e:
                print("except no {0}".format(e))
                foreign_investor = 0.0

        if self.operation == 5:
            return int(foreign_investor / 1000) > self.total
        elif self.operation == 3:
            return int(foreign_investor / 1000) < self.total
        elif self.operation == 1:
            return int(foreign_investor / 1000) == self.total
