class ForceSell(object):
    def __init__(self, details, selltimeperiod=10, operation=5, total=0):
        """
        主力連賣轉買
        :param details: 
        :param selltimeperiod: 
        :param operation: 
        :param total: 
        """
        self.details = details
        self.selltimeperiod = selltimeperiod
        self.total = total
        self.operation = operation

    def run(self):
        sold = False
        count = 0.0
        items = self.details[-self.selltimeperiod:-1]
        for detail in items:
            count += detail['buySell']

        if count < 0:
            if self.details[-1]['buySell'] > 0:
                sold = True

        return sold
