import talib


class OverBandUp(object):
    def __init__(self, price, time=20):
        """
        站上布林
        :param price: 
        :param timeperiods: 預設20日
        """
        self.price = price
        self.timeperiods = []
        self.timeperiods.append(time)

    def run(self):
        upper, middle, lower = talib.BBANDS(self.price, timeperiod=20, nbdevup=2.1, nbdevdn=2.1, matype=0)
        new_up = max(upper[-20:-1])
        is_new_up = False
        for index, p in enumerate(self.price[-20:-1]):
            if p < new_up:
                is_new_up = True
            else:
                is_new_up = False
                break
        return is_new_up
