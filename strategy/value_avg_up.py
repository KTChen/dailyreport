import talib


class ValueUp(object):
    def __init__(self, values, timeperiod=10, operation=5, multiple=1.5):
        """
        成交量放大
        :param values: 成交量
        :param timeperiod: 時間範圍 預設 20 日
        :param operation: 預設大於或等於
        :param multiple: 放量倍數 預設為放量1.5倍
        """
        self.values = values
        self.timeperiod = timeperiod
        self.operation = operation
        self.multiple = multiple

    def run(self):
        avgV = talib.SMA(self.values, timeperiod=self.timeperiod)[-1]
        if self.operation == 5:
            return self.values[-1] > (avgV * self.multiple)
        elif self.operation == 1:
            return False
        else:
            return False
