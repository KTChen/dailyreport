
class MainForce(object):
    def __init__(self, details, timeperiod=10, operation=5, total=0):
        """
        主力買超
        :param details: 
        :param timeperiod: 
        :param operation: 
        :param total: 
        """
        self.details = details
        self.timeperiod = timeperiod
        self.total = total
        self.operation = operation

    def run(self):

        buy_sell = 0.0
        for detail in self.details[-self.timeperiod:]:
            try:
                if 'buySell' in detail:
                    buy_sell += float(detail['buySell'])
            except Exception as e:
                print("except no {0}".format(e))
                buy_sell = 0.0

        if self.operation == 5:
            return int(buy_sell / 1000) > self.total
        elif self.operation == 1:
            return False
        else:
            return False
