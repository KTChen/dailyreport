#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
from _operator import itemgetter
from datetime import datetime
import requests
from pymongo import MongoClient
import pandas as pd
import cfscrape
import execjs
from urllib.parse import urljoin, urlparse, quote,unquote,urlencode
import time
from pyquery import PyQuery as jq
from hyper.contrib import HTTP20Adapter
from urllib import parse
import sys
from dateutil.relativedelta import relativedelta
client = MongoClient('127.0.0.1', int('27017'))
db = client['stock']
chip_db = db['chip']
chip_db2 = db['chip2']
chip_daily_db = db['chip_daily']
collectTWSE = db['twse_list']
collectStock = db['stock']
missed_chip = db['missed_chip']

headers = {
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"}


def delete_chips(stockNo):
    cr = chip_db.find_one({'stockNo': stockNo})
    details = cr['details']
    details.sort(key=itemgetter('date'), reverse=True)
    print(stockNo)

    df = pd.DataFrame(list(details))
    df['Date'] = pd.to_datetime(df['date'])
    df.set_index('Date', inplace=True)
    df.sort_index(inplace=True)
    df.head()

    end = datetime.strptime("20191017 23:59:59", '%Y%m%d %H:%M:%S')

    mask = (df['date'] <= end)

    df = df.loc[mask]
    stock_data = []
    for index, data in df.iterrows():
        chips = data['chips']
        chips_arr = []
        for chip in chips:
            chips_arr.append(chip)

        json_set = {'date': index,
                    'chips': chips_arr
                    }
        stock_data.append(json_set)

    chip_db.update({"stockNo": stockNo},
                   {'$set': {'details': stock_data}})


def checkTimes(level=3):
    timeStart = time.time()
    yield
    print(f"cost times: {round(time.time()-timeStart,level)}s")


def crack(content, url, end_time, save_res=False):

    scheme, netloc, *_ = urlparse(url)
    jq_data = jq(content)("#challenge-form")
    js_code = (
        jq(content)("script")
        .eq(0)
        .text()
        .replace("\n", "")
        .split("setTimeout(function(){")[1]
        .split("f.action += location.hash")[0]
        .replace("a.value", "res")
        .replace("t.length", str(len(url.replace(f"{scheme}://", "")) - 1))
        .split(";")
    )
    print(f"codes: {js_code}")
    key_data = json.loads(
        '{"'
        + js_code[0]
        .split(",")[-1]
        .replace('":', '":"')
        .replace("}", '"}')
        .replace("=", '":')
        + "}"
    )

    # warning(f"start_code: {key_data}")

    key_1 = list(key_data.keys())[0]
    key_2 = list(key_data[key_1].keys())[0]
    key_3 = f"{key_1}.{key_2}".strip()
    start_data = execjs.eval(js_code[0].split(",")[-1])

    print(key_1)
    print(key_2)
    print(key_3)

    print(f"{start_data[key_2]}: {js_code[0].split(',')[-1]}")

    ff = js_code[11:-3]
    for i in ff:
        try:
            code = i.replace(key_3, "")
            method = code[0]
            code = "{" + f'"{key_2}":{code[2:]}' + "}"
            start_data[key_2] = eval(
                f"start_data[key_2]{method}{execjs.eval(code)[key_2]}"
            )
            print(f"{start_data[key_2]}: {method} {code}")
        except Exception as e:
            pass

    params = {i.attr("name"): i.attr("value") for i in jq_data("input").items()}

    key2 = start_data[key_2]
    jc = js_code[-3]
    jc.replace(key_3, str(key2))

    params["jschl_answer"] = execjs.eval(jc)
    print(f'jschl_answer: {params["jschl_answer"]}')


    action = jq_data.attr("action")
    params['r'] = quote(params['r'])
    wait_times = round(end_time - time.time(), 3)
    if wait_times > 0:
        print(f"waiting for {wait_times} seconds")
        time.sleep(wait_times)
    redirect_url = urljoin(url, action)
    #redirect_url = f'{urljoin(url, action)}?{"&".join([f"{k}={v}" for k,v in params.items()])}'

    print(f"redirecting: {redirect_url}")
    return redirect_url, params

def test():
    url1 = "https://www.wantgoo.com/stock/astock/agentstat_ajax?StockNo={0}" \
           "&Types=1&StartDate={1}&EndDate={2}&Rows=9999".format(stock_no, date.strftime("%Y%m%d"),
                                                                 date.strftime("%Y%m%d"))

    session = requests.Session()
    session.mount(url1.split("/")[0], HTTP20Adapter())
    session.headers = {
        "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
        "accept-encoding": "gzip, deflate, br",
        "accept-language": "en-GB,en;q=0.9,zh-CN;q=0.8,zh;q=0.7",
        "cache-control": "no-cache",
        "dnt": "1",
        "pragma": "no-cache",
        "referer": url1,
        "sec-fetch-mode": "navigate",
        "sec-fetch-site": "same-origin",
        "sec-fetch-user": "?1",
        "upgrade-insecure-requests": "1",
        "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36",
    }

    resp = session.get(url1, allow_redirects=True)

    cookies = session.cookies.get_dict()
    __cfduid = cookies['__cfduid']
    end_time = time.time() + 4

    redirect_url, params = crack(resp.text, resp.url, end_time, save_res=True)

    data = parse.urlencode(params)
    session = requests.Session()
    session.mount(redirect_url.split("/")[0], HTTP20Adapter())
    session.headers = {
        "Host": "www.wantgoo.com",
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Accept-Language": "zh-TW,zh;q=0.8,en-US;q=0.5,en;q=0.3",
        "Accept-Encoding": "gzip, deflate, br",
        "Content-Type": "application/x-www-form-urlencoded",
        "Origin": 'https://www.wantgoo.com',
        "Connection": "keep-alive",
        "Referer": url1,
        "Cookie": "__cfduid=" + __cfduid,
        "upgrade-insecure-requests": "1",
        "TE": "Trailers",
    }


def download_daily(stock_no, date):

    new_date = datetime.strptime(date.strftime("%Y%m%d") + '00:00:00', '%Y%m%d%H:%M:%S')

    url1 = "https://www.wantgoo.com/stock/astock/agentstat_ajax?StockNo={0}" \
           "&Types=1&StartDate={1}&EndDate={2}&Rows=9999".format(stock_no, date.strftime("%Y%m%d"),
                                                                 date.strftime("%Y%m%d"))
    cookies = {
        'cf_clearance': 'f2ae65fd77c2cf58e39faa067f3096a303d1d395-1590661905-0-150',
        'BID': '0C26516D-647C-4CAF-92B0-7075BF595D28',
        '__cfduid': 'd3b68daf6aca592d4c59cbd6033c40e321590550099'
    }

    url2 = "https://www.wantgoo.com/Stock/aStock/AgentStat_Ajax?StockNo={0}&Types=2" \
           "&StartDate={1}" \
           "&EndDate={2}&Rows=9999".format(stock_no, date.strftime("%Y%m%d"), date.strftime("%Y%m%d"))

    res1 = requests.post(url1, headers=headers, cookies=cookies)
    res1.encoding = 'utf-8'
    data1 = json.loads(res1.text)['returnValues']

    print(data1)
    res2 = requests.get(url2, headers=headers, cookies=cookies)
    res2.encoding = 'utf-8'
    data2 = json.loads(res2.text)['returnValues']
    print(data2)
    arr = []

    for d in json.loads(data1):
        json_set = {'store': d['券商名稱'],
                    'buyValue': int(d['買量']),
                    'buyPrice': float(d['買價']),
                    'sellValue': int(d['賣量']),
                    'sellPrice': float(d['賣價']),
                    'buySell': int(d['買賣超']),
                    'avg': float(d['均價'])
                    }
        arr.append(json_set)

    for d in json.loads(data2):
        json_set = {'store': d['券商名稱'],
                    'buyValue': int(d['買量']),
                    'buyPrice': float(d['買價']),
                    'sellValue': int(d['賣量']),
                    'sellPrice': float(d['賣價']),
                    'buySell': int(d['買賣超']),
                    'avg': float(d['均價'])
                    }
        arr.append(json_set)

    item_set = {'date': new_date, 'chips': arr}

    return item_set


def get_database(details):
    new_data = {'2018Q1': [], '2018Q2': [], '2018Q3': [], '2018Q4': [], '2019Q1': [], '2019Q2': [], '2019Q3': [],
                '2019Q4': [], '2020Q1': [], '2020Q2': [], '2020Q3': [], '2020Q4': []}

    season = ''

    for data in details:
        stock_date = data['date']

        year = stock_date.year

        if 1 <= stock_date.month <= 3:
            season = 'Q1'
        elif 4 <= stock_date.month <= 6:
            season = 'Q2'
        elif 7 <= stock_date.month <= 9:
            season = 'Q3'
        elif 10 <= stock_date.month <= 12:
            season = 'Q4'

        dic = "{0}{1}".format(year, season)
        arr = new_data[dic]

        d = [data for data in arr if
             data.get('date').strftime("%Y-%m-%d 00:00:00") == stock_date.strftime(
                 "%Y-%m-%d 00:00:00")]
        if len(d) == 0:
            arr.append(data)


def main():

    items = []
    if len(sys.argv) == 2:
        if sys.argv[1] == 'missed':
            items = missed_chip.find({})
    else:
        items = collectTWSE.find({})

    for index, item in enumerate(items):
        try:

            stock_no = item['stock_id']

            chip_twse = db["chip_twse_2020Q2"]

            cr = chip_twse.find_one({'stockNo': stock_no})

            if cr is None:
                chip_twse.insert({"stockNo": stock_no, 'details': []})
                cr = chip_twse.find_one({'stockNo': stock_no})

            details = cr['details']

            for i in range(0, 1):
                # date = datetime.strptime("20191108 23:59:59", '%Y%m%d %H:%M:%S')
                new_date = datetime.today() - relativedelta(days=i)
                s = new_date.strftime("%Y%m%d")
                new_date = datetime.strptime(s + '00:00:00', '%Y%m%d%H:%M:%S')
                itm = collectStock.find_one({'stockNo': stock_no})

                if itm is not None:
                    stock_details = itm['details']
                    stock_details.sort(key=itemgetter('date'), reverse=True)

                    d = [data for data in stock_details if data.get('date') == new_date]
                    if len(d) == 0:
                        print('the date not open market : {0}'.format(new_date))
                        continue
                #new_date = datetime.strptime('2020052200:00:00', '%Y%m%d%H:%M:%S')

                d = [data for data in details if data.get('date') == new_date]

                if len(d) > 0:
                    print('date exist in database: {0}'.format(new_date))
                    continue

                items = download_daily(stock_no, new_date)
                details.append(items)

                details.sort(key=itemgetter('date'), reverse=True)
                chip_twse.update({"stockNo": stock_no},
                                    {'$set': {'details': []}})

                chip_twse.update({"stockNo": stock_no},
                                    {'$set': {'details': details}})

                print('insert stock_no: {0}, index: {1}, date: {2} '.format(stock_no, index, new_date))

        except Exception as e:
            print("download_daily Exception : stock id : {0}\r\n{1}\r\n".format('', e))



if __name__ == '__main__':
    main()
