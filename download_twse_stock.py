#!/usr/bin/env python
# -*- coding: utf-8 -*-

from operator import itemgetter
import random
import requests
import asyncio

import sys
from dateutil.relativedelta import relativedelta
from pymongo import MongoClient
import json
from datetime import datetime, timedelta
import time
import pandas_datareader as pdr
import csv
from pygments import StringIO
import pandas as pd
import configparser
from twstock import Stock

client = MongoClient('127.0.0.1', int('27017'))
db = client['stock']
collectStock = db['stock']
collectTWSE = db['twse_list']
collectOTC = db['otc_list']
chip_db = db['chip']

sleep_time = 5.0

dateTime = ''
tw_dateTime = ''
insert_json_array = []
headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36"}


def download＿specific_twse_history(checked, downloadDate):
    items = collectTWSE.find({'type': 'twse'})
    for index, item in enumerate(items):

        stock_no = item['stock_id']
        stock_name = item['stock_name']
        json_set_array = []

        check = collectStock.find_one({'stockNo': stock_no})

        # 檢查是否存在
        if checked and check is not None:
            continue
        new_date = datetime.strptime('2019102300:00:00', '%Y%m%d%H:%M:%S')
        details = check['details']
        details.sort(key=itemgetter('date'), reverse=True)

        db_date = details[0]['date']
        if db_date > new_date:
            continue

        url = "https://www.twse.com.tw/exchangeReport/STOCK_DAY?" \
              "response=json&date={0}&stockNo={1}".format(downloadDate.strftime("%Y%m%d"), stock_no)

        time.sleep(sleep_time)
        result = requests.get(url)
        stocks = json.loads(result.text)
        print(stocks)

        stat = stocks['stat']
        if stat != 'OK':
            continue
        datas = stocks['data']

        if len(details) == 0:
            print('No Data - {0}'.format(stock_no))
            continue

        for ritem in datas:
            dates = str(ritem[0]).split('/')
            year = int(dates[0]) + 1911
            date = '{0}{1}{2}'.format(year, dates[1], dates[2])
            new_date = datetime.strptime(date + '00:00:00', '%Y%m%d%H:%M:%S')

            if details[0]['date'] < new_date:
                vol = ritem[1].replace('--', '0').replace(',', '')
                open = ritem[3].replace('--', '0').replace(',', '')
                high = ritem[4].replace('--', '0').replace(',', '')
                low = ritem[5].replace('--', '0').replace(',', '')
                close = ritem[6].replace('--', '0').replace(',', '')

                json_set = {'date': new_date,
                            'dealValue': float(vol),
                            'openPrice': float(open),
                            'highPrice': float(high),
                            'lowPrice': float(low),
                            'closePrice': float(close)
                            }

                details.insert(0, json_set)

                collectStock.update({"stockNo": stock_no},
                                    {'$set': {'details': []}})

                collectStock.update({"stockNo": stock_no},
                                    {'$set': {'details': details}})

                print('insert {0} - {1}'.format(stock_no, new_date))
                json_set_array.append(json_set)


def download_otc_history():
    items = collectTWSE.find({'type': 'otc'})

    for index, item in enumerate(items):
        stock_no = item['stock_id']

        check = collectStock.find_one({'stockNo': stock_no})
        details = check['details']
        details.sort(key=itemgetter('date'), reverse=True)

        for i in range(0, 1):
            date = datetime.today() - relativedelta(months=i)

            items = download_otc(stock_no, date)

            for itm in items:
                new_date = itm['date']
                d = [data for data in details if data.get('date') == new_date]

                if len(d) > 0:
                    print('date existing: {0}'.format(new_date))
                    continue
                details.append(itm)
                print('insert stock_no: {0}, index: {1}, details len: {2}, date: {3}'.format(stock_no, index, len(details), new_date))
            time.sleep(5)

        details.sort(key=itemgetter('date'), reverse=True)
        collectStock.update({"stockNo": stock_no},
                            {'$set': {'details': []}})

        collectStock.update({"stockNo": stock_no},
                            {'$set': {'details': details}})


def parse_otc(data, stock_no):
    csv_file = csv.reader(StringIO(data))
    items = list(csv_file)
    numline = len(items)
    array_items = []
    for index, item in enumerate(items):

        if '共' in item[0] and '筆' in item[0]:
            break

        if 4 < index < numline:
            ritem = [t.replace('--', '0').replace(',', '').replace('＊', '') for t in item]
            dates = str(ritem[0]).split('/')

            year = int(dates[0]) + 1911
            date = '{0}{1}{2}'.format(year, dates[1], dates[2])

            new_date = datetime.strptime(date + '00:00:00', '%Y%m%d%H:%M:%S')

            float(ritem[1])

            json_set = {'date': new_date,
                        'dealValue': float(ritem[1]) * 1000,
                        'openPrice': float(ritem[3]),
                        'highPrice': float(ritem[4]),
                        'lowPrice': float(ritem[5]),
                        'closePrice': float(ritem[6])
                        }
            array_items.append(json_set)

    return array_items


def download_otc(stock_no, date):
    url = (
                  'http://www.tpex.org.tw/ch/stock/aftertrading/' +
                  'daily_trading_info/st43_download.php?d=%(year)d/%(mon)02d&' +
                  'stkno=%(stock)s&r=%(rand)s') % {
              'year': date.year - 1911,
              'mon': date.month,
              'stock': stock_no,
              'rand': random.randrange(1, 1000000)}

    result = requests.post(url)
    print(result.text)
    return parse_otc(result.text, stock_no)


def download_twse_history():
    items = collectTWSE.find({'type': 'twse'})
    for index, item in enumerate(items):

        stock_no = item['stock_id']

        stock_name = item['stock_name']
        json_set_array = []

        # check = collectStock.find_one({'stockNo': stock_no})
        #
        # if check is not None:
        #     continue

        check = collectStock.find_one({'stockNo': stock_no})
        details = check['details']
        details.sort(key=itemgetter('date'), reverse=True)

        for i in range(0, 1):

            # date = datetime.strptime("20191108 23:59:59", '%Y%m%d %H:%M:%S')
            date = datetime.today() - relativedelta(months=i)
            url = "https://www.twse.com.tw/exchangeReport/STOCK_DAY?" \
                  "response=json&date={0}&stockNo={1}".format(date.strftime("%Y%m%d"), stock_no)

            time.sleep(sleep_time)
            result = requests.get(url)
            stocks = json.loads(result.text)

            stat = stocks['stat']
            if stat != 'OK':
                continue
            datas = stocks['data']

            for ritem in datas:

                dates = str(ritem[0]).split('/')
                year = int(dates[0]) + 1911
                date = '{0}{1}{2}'.format(year, dates[1], dates[2])
                new_date = datetime.strptime(date + '00:00:00', '%Y%m%d%H:%M:%S')

                d = [data for data in details if data.get('date') == new_date]

                if len(d) > 0:
                    print('date existing: {0}'.format(new_date))
                    continue

                vol = ritem[1].replace('--', '0').replace(',', '')
                open = ritem[3].replace('--', '0').replace(',', '')
                high = ritem[4].replace('--', '0').replace(',', '')
                low = ritem[5].replace('--', '0').replace(',', '')
                close = ritem[6].replace('--', '0').replace(',', '')

                json_set = {'date': new_date,
                            'dealValue': float(vol),
                            'openPrice': float(open),
                            'highPrice': float(high),
                            'lowPrice': float(low),
                            'closePrice': float(close)
                            }
                print('insert stock_no: {0}, index: {1}, details len: {2}, date: {3}'.format(stock_no, index, len(details), new_date))
                details.insert(0, json_set)

        details.sort(key=itemgetter('date'), reverse=True)
        collectStock.update({"stockNo": stock_no},
                            {'$set': {'details': []}})

        collectStock.update({"stockNo": stock_no},
                            {'$set': {'details': details}})


def update_otc_value():
    items = collectTWSE.find({'type': 'otc'})

    for itm in items:

        item = collectStock.find_one({'stockNo': itm['stock_id']})
        stockNo = item['stockNo']
        details = item['details']

        if len(details) == 0:
            continue

        df = pd.DataFrame(list(details))
        df['Date'] = pd.to_datetime(df['date'])
        df.set_index('Date', inplace=True)
        df.sort_index(inplace=True)
        df.head()

        end = datetime.strptime("20171109 23:59:59", '%Y%m%d %H:%M:%S')

        mask = (df['date'] <= end)

        df = df.loc[mask]
        stock_data = []
        for index, data in df.iterrows():
            json_set = {'date': index,
                        'dealValue': float(data['dealValue'] * 1000),
                        'openPrice': float(data['openPrice']),
                        'highPrice': float(data['highPrice']),
                        'lowPrice': float(data['lowPrice']),
                        'closePrice': float(data['closePrice']),
                        }
            stock_data.append(json_set)
        collectStock.update({"stockNo": stockNo},
                            {'$set': {'details': stock_data}})

        print(stockNo)


def delete_stock_all():
    items = collectTWSE.find({'type': 'otc'})

    for itm in items:

        item = collectStock.find_one({'stockNo': itm['stock_id']})

        if item is None:
            continue
        stockNo = item['stockNo']
        collectStock.delete_one({"stockNo": stockNo})
        print(stockNo)


def delete_all_stock_item():
    items = collectStock.find({"type": "twse"})
    for itm in items:
        d = collectStock.delete_one({'stockNo': itm['stockNo']})
        print(d.raw_result)


def delete_stock_item():
    items = collectTWSE.find({"type": "twse"})

    for itm in items:

        item = collectStock.find_one({'stockNo': itm['stock_id']})

        if item is None:
            continue
        stockNo = item['stockNo']
        details = item['details']

        if len(details) == 0:
            continue
        df = pd.DataFrame(list(details))
        df['Date'] = pd.to_datetime(df['date'])
        df.set_index('Date', inplace=True)
        df.sort_index(inplace=True)
        df.head()

        end = datetime.strptime("20200514 23:59:59", '%Y%m%d %H:%M:%S')

        mask = (df['date'] <= end)

        df = df.loc[mask]
        stock_data = []
        for index, data in df.iterrows():
            json_set = {'date': index,
                        'dealValue': float(data['dealValue']),
                        'openPrice': float(data['openPrice']),
                        'highPrice': float(data['highPrice']),
                        'lowPrice': float(data['lowPrice']),
                        'closePrice': float(data['closePrice']),
                        }
            stock_data.append(json_set)
        stock_data.sort(key=itemgetter('date'), reverse=True)
        collectStock.update({"stockNo": stockNo},
                            {'$set': {'details': stock_data}})

        print(stockNo)


def download_twse_daily():
    url = "http://www.twse.com.tw/exchangeReport/STOCK_DAY_ALL?response=csv"
    result = requests.get(url)
    csv_file = csv.reader(StringIO(result.text))
    csv_list = list(csv_file)
    items = csv_list[2:]
    cdate = csv_list[0][0]

    year = int(cdate[0:3]) + 1911
    month = int(cdate[4:6])
    day = int(cdate[7:9])

    today = datetime(year=year, month=month, day=day)

    for item in items:

        if '說明:' in item[0]:
            break

        stockNo = item[0].replace('=', '').replace('"', '').rstrip()
        values = float(item[2].replace(',', '').rstrip())
        openPrice = float(item[4].replace(',', '').rstrip())
        highPrice = float(item[5].replace(',', '').rstrip())
        lowPrice = float(item[6].replace(',', '').rstrip())
        closePrice = float(item[7].replace(',', '').rstrip())
        itm = collectStock.find_one({'stockNo': stockNo})

        if itm is not None:
            details = itm['details']
            details.sort(key=itemgetter('date'), reverse=True)

            d = [data for data in details if data.get('date') == today]

            if len(d) > 0:
                print('date existing: {0}'.format(new_date))
                continue

            if len(details) == 0:
                print('No Data - {0}'.format(stockNo))
                continue

            db_date = details[0]['date'].strftime("%Y-%m-%d 00:00:00")
            if db_date == today.strftime("%Y-%m-%d 00:00:00"):
                print("Data exist - {0}".format(stockNo))
                continue
            else:
                s = datetime.today().strftime("%Y%m%d")

                new_date = datetime.strptime(s + '00:00:00', '%Y%m%d%H:%M:%S')

                json_set = {'date': new_date,
                            'dealValue': values,
                            'openPrice': openPrice,
                            'highPrice': highPrice,
                            'lowPrice': lowPrice,
                            'closePrice': closePrice
                            }
                details.insert(0, json_set)

                collectStock.update({"stockNo": stockNo},
                                    {'$set': {'details': []}})

                collectStock.update({"stockNo": stockNo},
                                    {'$set': {'details': details}})

                print('insert {0} - {1}'.format(stockNo, datetime.today()))


def download_otc_daily(date=datetime.today()):
    url = "http://www.tpex.org.tw/web/stock/aftertrading/daily_close_quotes/stk_quote_download.php" \
          "?l=zh-tw&d={0}/{1}/{2}&s=0,asc,0".format(date.year - 1911, "%02d" % date.month, "%02d" % date.day)
    result = requests.get(url)

    data = result.content
    _de = data.decode('cp950', 'ignore')

    csv_file = csv.reader(StringIO(_de))
    csv_list = list(csv_file)
    items = csv_list[3:]
    cdate = csv_list[1][0].split(":")[1]

    year = int(cdate[0:3]) + 1911
    month = int(cdate[4:6])
    day = int(cdate[7:9])

    today = datetime(year=year, month=month, day=day)

    stocks = collectTWSE.find({'type': 'otc'})
    stockNos = []

    for s in stocks:
        stockNos.append(s['stock_id'])

    for index, item in enumerate(items):

        if len(item) == 0:
            break

        if '說明:' in item[0]:
            break
        stockNo = item[0].replace('=', '').replace('"', '').rstrip()

        if stockNo not in stockNos:
            print("item {0} not exist".format(stockNo))
            continue

        closePrice = 0
        openPrice = 0
        highPrice = 0
        lowPrice = 0
        values = 0

        if '--' not in item[2]:
            closePrice = float(item[2].replace(',', '').rstrip())
        if '--' not in item[4]:
            openPrice = float(item[4].replace(',', '').rstrip())
        if '--' not in item[5]:
            highPrice = float(item[5].replace(',', '').rstrip())
        if '--' not in item[6]:
            lowPrice = float(item[6].replace(',', '').rstrip())
        if '--' not in item[8]:
            values = float(item[8].replace(',', '').rstrip())

        itm = collectStock.find_one({'stockNo': stockNo})

        if itm is not None:
            details = itm['details']

            if len(list(details)) == 0:
                continue
            details.sort(key=itemgetter('date'), reverse=True)
            db_date = details[0]['date'].strftime("%Y-%m-%d 00:00:00")
            # today = date.strftime("%Y-%m-%d 00:00:00")
            if db_date == today.strftime("%Y-%m-%d 00:00:00"):
                print("Data exist - " + stockNo)
                continue
            else:
                s = date.strftime("%Y%m%d")

                new_date = datetime.strptime(s + '00:00:00', '%Y%m%d%H:%M:%S')
                json_set = {'date': new_date,
                            'dealValue': values,
                            'openPrice': openPrice,
                            'highPrice': highPrice,
                            'lowPrice': lowPrice,
                            'closePrice': closePrice
                            }
                details.insert(0, json_set)

                collectStock.update({"stockNo": stockNo},
                                    {'$set': {'details': []}})

                collectStock.update({"stockNo": stockNo},
                                    {'$set': {'details': details}})

                print('insert {0} - {1}'.format(stockNo, new_date))


def main():
    if len(sys.argv) > 0:
        if sys.argv[1] == 'twse':
            if len(sys.argv) > 1:
                if sys.argv[2] == 'history':
                    download_twse_history()
                elif sys.argv[2] == 'specific':
                    download＿specific_twse_history(False, datetime.now())
                elif sys.argv[2] == 'delete':
                    delete_stock_item()
                else:
                    download_twse_daily()
        elif sys.argv[1] == 'otc':
            if len(sys.argv) > 1:
                if sys.argv[2] == 'history':
                    download_otc_history()
                else:
                    download_otc_daily(datetime.today())
        elif sys.argv[1] == 'delete':
            if sys.argv[2] == 'all':
                delete_all_stock_item()
            else:
                delete_stock_item()


if __name__ == '__main__':
    main()
