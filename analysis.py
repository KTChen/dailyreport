#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from operator import itemgetter
from pymongo import MongoClient
import sys
import talib
import pandas as pd
import configparser
import numpy


client = MongoClient('127.0.0.1', int('27017'))
db = client['stock']
collect = db['stock']
major = db['major']
obs_analysis_tabel = db['obs_analysis']
chip_db = db['chip']
real_time = db['real_time']
agents = db['agents']
daily_analysis_collect = db['daily_analysis']
low_analysis_collect = db['low_analysis']
daily_analysis_collect2 = db['daily_analysis2']
monthly_value_collect = db['monthly_value']

collectTWSE = db['twse_list']
collectOTC = db['otc_list']
missed_chip = db['missed_chip']


class Analysis(object):
    targets = []

    def __init__(self, targets=None):
        self.targets = targets

    def analysis_major(self, stock, date_str=None):
        item = major.find_one({'stockNo': stock})

        if item is None:
            return

        stock = item['stockNo']

        if date_str is None:
            date_str = datetime.today().strftime("%Y%m%d")

        data, ary = find_major(stock, date_str)
        return data

    def analysis_chip(self, stock, date_str=None):
        item = collect.find_one({'stockNo': stock})
        stock = item['stockNo']
        name = item['stockName']
        if item is None:
            return

        if date_str is None:
            date_str = datetime.today().strftime("%Y%m%d")

        details = item['details']
        df = pd.DataFrame(list(details))
        df['Date'] = pd.to_datetime(df['date'])
        df.set_index('Date', inplace=True)
        df.sort_index(inplace=True)
        df.head()
        end = datetime.strptime(date_str + " 23:59:59", '%Y%m%d %H:%M:%S')
        mask = (df['date'] <= end)
        df = df.loc[mask]

        r = collectTWSE.find_one({'stock_id': '{0}'.format(stock)})

        data = '{0}\r\n收盤價:{1} 開盤:{2}\r\n最高:{3} 最低:{4}'.format(
            df['date'][-1].strftime("%Y-%m-%d"),
            df['closePrice'][-1],
            df['openPrice'][-1],
            df['highPrice'][-1],
            df['lowPrice'][-1]
        )

        return name, stock, r, data

    def analysis_less_chip(self, stock, date_str=None):
        item = collect.find_one({'stockNo': stock})
        stock = item['stockNo']
        name = item['stockName']
        if item is None:
            return

        if date_str is None:
            date_str = datetime.today().strftime("%Y%m%d")

        details = item['details']
        df = pd.DataFrame(list(details))
        df['Date'] = pd.to_datetime(df['date'])
        df.set_index('Date', inplace=True)
        df.sort_index(inplace=True)
        df.head()
        end = datetime.strptime(date_str + " 23:59:59", '%Y%m%d %H:%M:%S')
        mask = (df['date'] <= end)
        df = df.loc[mask]

        data1 = find_agents(stock, date_str, 1)

        data20 = find_agents(stock, date_str, 20)

        data60 = find_agents(stock, date_str, 60)

        data120 = find_agents(stock, date_str, 120)

        data = '日期 {0}\r1/20/60/120日\r\n{1}%/{2}%/{3}%/{4}%'.format(
            df['date'][-1].strftime("%Y-%m-%d"),
            "%.2f" % data1['total'],
            "%.1f" % data20['total'],
            "%.1f" % data60['total'],
            "%.1f" % data120['total']
        )

        return data

    def analysis_stock(self, stock, date_str=None):
        item = collect.find_one({'stockNo': stock})
        stock = item['stockNo']
        name = item['stockName']
        if item is None:
            return

        if date_str is None:
            date_str = datetime.today().strftime("%Y%m%d")

        details = item['details']
        df = pd.DataFrame(list(details))
        df['Date'] = pd.to_datetime(df['date'])
        df.set_index('Date', inplace=True)
        df.sort_index(inplace=True)
        df.head()
        end = datetime.strptime(date_str + " 23:59:59", '%Y%m%d %H:%M:%S')
        mask = (df['date'] <= end)
        df = df.loc[mask]

        r = collectTWSE.find_one({'stock_id': '{0}'.format(stock)})

        data1 = find_agents(stock, date_str, 1)

        data20 = find_agents(stock, date_str, 20)

        data60 = find_agents(stock, date_str, 60)

        data120 = find_agents(stock, date_str, 120)

        data = '{0}-{1}-{2}\r\n\r\n日期 {3}\r\n收盤價 {4}\r\n開盤 {5}\r\n最高 {6}\r\n最低 {7} \r\n\r\n買超：{16} \r\n賣超：{17}' \
               '\r\n\r\n佔股本比重\r\n1/20/60/120日\r\n{8}%/{9}%/{10}%/{11}%\r\n\r\n1/20/60/120均價\r\n{12}/{13}/{14}/{15}\r\n\r\n籌碼集中度\r\n{18}%/{19}%/{20}%/{21}%'.format(
            name, stock, r['type'],
            df['date'][-1].strftime("%Y-%m-%d"),
            df['closePrice'][-1],
            df['openPrice'][-1],
            df['highPrice'][-1],
            df['lowPrice'][-1],
            "%.2f" % data1['total'],
            "%.1f" % data20['total'],
            "%.1f" % data60['total'],
            "%.1f" % data120['total'],
            "%.1f" % data1['avg'],
            "%.1f" % data20['avg'],
            "%.1f" % data60['avg'],
            "%.1f" % data120['avg'],
            data1['buy'],
            data1['sell'],
            "%.1f" % data1['concentrated'],
            "%.1f" % data20['concentrated'],
            "%.1f" % data60['concentrated'],
            "%.1f" % data120['concentrated']

        )

        return data

    def find(self):

        items = {}
        twse = collect.find({"stockNo": {"$in": self.targets}})
        for index, item in enumerate(twse):
            up, low = obv(item, "20171103")
            items.update({item['stockNo']: up})

        return items

    def chip_analysis(self):
        twse = collect.find({})
        for index, item in enumerate(twse):
            try:
                if 'details' not in item.keys():
                    continue
                chip_analysis(item, "20171103")

            except Exception as e:
                print("{0}-{1}".format(item['stockNo'], e))

    def analysis_daily(self, stockId):
        twse = collect.find({})
        for index, item in enumerate(twse):
            try:
                if 'details' not in item.keys():
                    continue
                daily_analysis(item, "20171103")
            except Exception as e:
                print("{0}-{1}".format(item['stockNo'], e))

    def get_obs(self):
        items = obs_analysis_tabel.find({})
        df = pd.DataFrame(list(items))
        df['Date'] = pd.to_datetime(df['createDate'])
        df.set_index('Date', inplace=True)
        df.sort_index(inplace=True)
        df.head()

        end = datetime.strptime(df['createDate'][-1].strftime("%Y%m%d") + " 00:00:00", '%Y%m%d %H:%M:%S')

        mask = (df['createDate'] >= end)

        df = df.loc[mask]
        return df

    def get_daily(self):
        items = daily_analysis_collect.find({})
        df = pd.DataFrame(list(items))
        df['Date'] = pd.to_datetime(df['createDate'])
        df.set_index('Date', inplace=True)
        df.sort_index(inplace=True)
        df.head()

        end = datetime.strptime(df['createDate'][-1].strftime("%Y%m%d") + " 00:00:00", '%Y%m%d %H:%M:%S')

        mask = (df['createDate'] >= end)

        df = df.loc[mask]
        return df


def sell(details, index, day):
    """
    回測 賣出
    :param details:
    :param index:
    :param day:
    :return:
    """
    buy = details[index]['closePrice']
    buyDate = details[index]['date']
    sellPrice = details[index:index + day][-1]['closePrice']
    sellDate = details[index:index + day][-1]['date']

    m = (sellPrice - buy)
    p = (m / buy) * 100
    print("Date:{0} - buy:{1} --- Date:{2} - sell:{3}  {4}".format('{0:%Y%m%d}'.format(buyDate), buy,
                                                                   '{0:%Y%m%d}'.format(sellDate), sellPrice, p))


def find_agents(stockNo, date, delta):
    """
    籌碼買超
    :param agents:
    :param stockNo:
    :param date:
    :return:
    """
    if stockNo == '1101':
        print('')

    stock = collectTWSE.find_one(
        {'stock_id': stockNo})

    if stock is None:
        return None

    stockChip = 0
    if 'chip' in stock:
        stockChip = int(stock['chip']) / 1000

    end = datetime.strptime(date + " 23:59:59", '%Y%m%d %H:%M:%S')

    info_result = collect.find_one({'stockNo': stockNo})

    chip_twse_Q2 = db["chip_twse_2020Q2"]
    chip_twse_Q1 = db["chip_twse_2020Q1"]
    result2 = chip_twse_Q2.find_one({'stockNo': stockNo})
    result1 = chip_twse_Q1.find_one({'stockNo': stockNo})

    if info_result is None:
        print(stockNo + ' - Data not existing')
        missed_chip.insert({'stock_id': stockNo})
        return None

    details1 = []
    details2 = []

    if result1 is not None:
        details1 = result1['details']

    if result2 is not None:
        details2 = result2['details']

    details = details1 + details2
    info_details = info_result['details']


    info_details.sort(key=itemgetter('date'), reverse=True)
    details.sort(key=itemgetter('date'), reverse=True)

    info_detailsDataFrame = pd.DataFrame(list(info_details))

    detailsDataFrame = pd.DataFrame(list(details))

    info_mask = (info_detailsDataFrame['date'] < end)

    mask = (detailsDataFrame['date'] < end)

    detailsDataFrame = detailsDataFrame.loc[mask][:delta]
    info_detailsDataFrame = info_detailsDataFrame.loc[info_mask][:delta]
    df_array = []

    chips = detailsDataFrame['chips'].tolist()

    for detail in chips:
        for chip in detail:
            df_array.append(chip)

    data = {}

    if len(df_array) == 0:
        data['stockChip'] = 0
        data['total'] = 0
        data['avg'] = 0
        data['buyStore'] = 0
        data['sellStore'] = 0
        data['buy'] = 0
        data['sell'] = 0
        data['concentrated'] = 0
        return data

    df = pd.DataFrame(list(df_array))

    groupby = df.groupby('store').agg(
        {'avg': 'mean', 'buySell': 'sum', 'buyValue': 'sum', 'sellValue': 'sum'}).sort_values(['buySell'],
                                                                                              ascending=[False])

    # 前15買超
    groupBuy = groupby[0:15]
    # 前15賣超
    groupSell = groupby[-15:]
    buyStore = groupby[0:3]
    sellStore = groupby[-3:].sort_values(['buySell'], ascending=[True])

    buy = groupBuy['buySell'].sum()
    sell = groupSell['buySell'].sum()
    avg = groupby['avg'].mean()

    totalValues = info_detailsDataFrame['dealValue'].sum()

    bs = (buy + sell)
    concentrated = (bs / totalValues) * 100

    if stockChip == 0:
        data['stockChip'] = 0
        data['total'] = 0
    else:
        data['stockChip'] = stockChip
        data['total'] = (bs / stockChip) * 100

    data['avg'] = avg
    data['buyStore'] = buyStore
    data['sellStore'] = sellStore
    data['buy'] = buy
    data['sell'] = sell
    data['concentrated'] = concentrated
    return data


def find_major(stockNo, date):
    """
    大戶買賣超
    :param major:
    :param stockNo:
    :param date:
    :return:
    """
    result = major.find_one({'stockNo': stockNo})

    if result is None:
        return None

    details = result['details']
    details.sort(key=itemgetter('date'), reverse=True)

    detailsDataFrame = pd.DataFrame(list(details))

    end = datetime.strptime(date + " 23:59:59", '%Y%m%d %H:%M:%S')

    mask = (detailsDataFrame['date'] <= end)
    df = detailsDataFrame.loc[mask][:3]

    data = "{0}-{1}".format(result['stockName'], result['stockNo'])

    data_ary = []

    for index, row in df.iterrows():
        ds = {'date': row[1], 'fund': row[0], 'major': row[2], 'supervisor': row[3]}
        data_ary.append(ds)
        data += "\r\n日期: {0}\r\n籌碼集中度: {1}\r\n外資籌碼: {2}\r\n大戶籌碼: {3}\r\n" \
            .format(row[1].strftime("%Y-%m-%d"), row[0], row[2], row[3])

    return data, data_ary


def bbandUp(close_price, up, low, bandratemid):
    """
    在上通道中
    :param bandrate 大於通道比率:
    :return:
    """
    uuppp = (up - low)

    if uuppp != 0:
        b = (close_price - low) / uuppp
        return b > bandratemid
    else:
        return False


def bbandLow(close_price, up, low):
    """
    靠近下通道
    :param bandrate 通道比率:
    :return:
    """
    uuppp = (up - low)

    if uuppp != 0:
        b = (close_price - low) / uuppp
        return b < 0.3
    else:
        return False


def avgValue(df, date, delta):
    end = datetime.strptime(date + " 23:59:59", '%Y%m%d %H:%M:%S')
    mask = (df['date'] <= end)
    df = df.loc[mask]

    dv = df['dealValue'].fillna(0).tolist()

    today_value = dv[-1]

    avgV = talib.SMA(numpy.array(dv), timeperiod=delta)
    avgValue = (avgV[-1])
    return today_value > avgValue * 1.5


def bband_threshold_mid(df, mid):
    """
    穿越布林中軌
    :param df:
    :param upper:
    :param lower:
    :param timeperiod:
    :return:
    """
    close_items = df['closePrice'].tolist()

    pre = close_items[-2]
    now = close_items[-1]

    return pre < mid[-1] < now


def bbandzip(df, upper, lower, timeperiod):
    """
    布林壓縮
    :param details:
    :param upper:
    :param lower:
    :param index:
    :param timeperiod: 天數
    :return:
    """
    bzip = True

    itm = df[-1:]

    sma5 = talib.SMA(numpy.array(df['closePrice'].tolist()), timeperiod=5)

    sma10 = talib.SMA(numpy.array(df['closePrice'].tolist()), timeperiod=10)

    sma20 = talib.SMA(numpy.array(df['closePrice'].tolist()), timeperiod=20)

    a = numpy.array([sma5[-5:],
                     sma10[-5:],
                     sma20[-5:]])
    h = numpy.max(a)
    l = numpy.min(a)


    if l == 0:
        return
    sideway = 100 * (h - l) / l

    close_items = itm['closePrice'].tolist()
    high_items = itm['highPrice'].tolist()
    upper = upper[-timeperiod:-1]
    lower = lower[-timeperiod:-1]

    for index, close in enumerate(close_items):

        up = upper[index]
        if close > up:
            bzip = False
            break
        else:
            continue

    for index, high in enumerate(high_items):

        up = upper[index]
        if high > up:
            bzip = False
            break
        else:
            continue

    if sideway < 0.5:
        bzip = False

    return bzip


def lower_shadows(close_price, open_price, low_price, high_price):
    low = ((min(close_price, open_price) - low_price) / low_price) * 100

    real = (max(close_price, open_price) - min(close_price, open_price)) / min(close_price, open_price) * 100

    high = (high_price - max(close_price, open_price)) / max(close_price, open_price) * 100

    if low > (real * 2):
        if high > real:
            if real < 5:
                if close_price > open_price:
                    return True

    return False


def real_red(detail):
    """
    實體長紅
    :param detail:
    :return:
    """
    low = detail['lowPrice'].tolist()[-1]
    high = detail['highPrice'].tolist()[-1]
    close = detail['closePrice'].tolist()[-1]
    return close > (high + low) / 2


def over_60ma(df, timeperiod=60):
    items = df['closePrice'].tolist()

    sma = talib.SMA(numpy.array(items), timeperiod=timeperiod)

    return items[-1] > sma[-1]


def take_first(array_like):
    return array_like[-1]


def take_last(array_like):
    return array_like[1]


def obv(item, date_str):
    item_count = 120

    stock = item['stockNo']

    details = item['details']

    if len(details) == 0:
        return

    details.sort(key=itemgetter('date'), reverse=False)

    df = pd.DataFrame(list(details))
    df['Date'] = pd.to_datetime(df['date'])
    df.set_index('Date', inplace=True)
    df.sort_index(inplace=True)
    df.head()

    end = datetime.strptime(date_str + " 23:59:59", '%Y%m%d %H:%M:%S')

    mask = (df['date'] <= end)

    df = df.loc[mask][-item_count:]

    close_list = df.fillna(0).closePrice.tolist()

    if len(close_list) == 0:
        return

    upper, middle, lower = talib.BBANDS(numpy.array(close_list),
                                        timeperiod=20, nbdevup=2.1,
                                        nbdevdn=2.1, matype=0)
    if stock is None:
        return
    up = upper[-1]
    low = lower[-1]

    return up, low


def chip_analysis(item, date_str, simulation=False):
    item_count = 120


    stock = item['stockNo']
    print('analysis : ' + stock)
    name = item['stockName']

    details = item['details']

    if len(details) == 0:
        return

    details.sort(key=itemgetter('date'), reverse=False)

    df = pd.DataFrame(list(details))
    df['Date'] = pd.to_datetime(df['date'])
    df.set_index('Date', inplace=True)
    df.sort_index(inplace=True)
    df.head()

    end = datetime.strptime(date_str + " 23:59:59", '%Y%m%d %H:%M:%S')

    mask = (df['date'] <= end)

    df = df.loc[mask][-item_count:]

    closeList = df.fillna(0).closePrice.tolist()
    if len(closeList) == 0:
        return
    if stock is None:
        return

    upper, middle, lower = talib.BBANDS(numpy.array(closeList),
                                        timeperiod=20, nbdevup=2.1,
                                        nbdevdn=2.1, matype=0)
    if over_60ma(df):
        if bbandzip(df, upper, lower, 20):
            data20 = find_agents(stock, date_str, 20)
            if data20 is not None and data20['total'] > 1:
                data60 = find_agents(stock, date_str, 60)
                if data20 is not None and data60['total'] > 3:
                    data5 = find_agents(stock, date_str, 5)
                    data10 = find_agents(stock, date_str, 10)

                    r = collectTWSE.find_one({'stock_id': '{0}'.format(stock)})
                    item = {
                        'createDate': df['date'][-1],
                        'stockName': name,
                        'type': r['type'],
                        'stockNo': stock,
                        'closePrice': df['closePrice'][-1],
                        'openPrice': df['openPrice'][-1],
                        'highPrice': df['highPrice'][-1],
                        'lowPrice': df['lowPrice'][-1],
                        'stockChip5': data5['stockChip'],
                        'stockChip10': data10['stockChip'],
                        'stockChip': data20['stockChip'],
                        'stockChip60': data60['stockChip'],
                        'total5': data5['total'],
                        'total10': data10['total'],
                        'total20': data20['total'],
                        'total60': data60['total'],
                        'avg5': data5['avg'],
                        'avg10': data10['avg'],
                        'avg20': data20['avg'],
                        'avg60': data60['avg'],
                        'concentrated5': data5['concentrated'],
                        'concentrated10': data10['concentrated'],
                        'concentrated20': data20['concentrated'],
                        'concentrated60': data60['concentrated'],
                        'buyDate': None,
                        'buyPrice': 0.0,
                        'sellDate': None,
                        'sellPrice': 0.0,
                        'wpct': 0.0,
                        'isSell': False,
                        'cost': 0
                    }
                    obs_analysis_tabel.insert_one(item)
                    print("{0}".format(stock))


def moving_average(values, window):
    weights = np.repeat(1.0, window) / window
    smas = np.convolve(values, weights, 'valid')
    return smas


def reset_history():
    items = daily_analysis_collect.find({})

    for item in items:
        daily_analysis_collect.update({"stockNo": item['stockNo'], 'date': item['date']},
                                      {'$set': {'wpct': 0, 'isSell': False, 'sellPrice': 0, 'sellDate': None}})


def history_analysis_chip(hitsoryitem):
    stockNo = hitsoryitem['stockNo']
    item = collect.find_one({'stockNo': stockNo})

    if item is not None:
        stockName = item['stockName']
        details = item['details']
        if details is not None and len(details) > 0:
            details.sort(key=itemgetter('date'), reverse=False)
            df = pd.DataFrame(list(details))
            df['Date'] = pd.to_datetime(df['date'])
            df.set_index('Date', inplace=True)
            df.sort_index(inplace=True)
            df.head()
            close_list = df.fillna(0).closePrice.tolist()
            sma10 = talib.SMA(numpy.array(close_list), timeperiod=10)
            high = df['highPrice'].tolist()
            low = df['lowPrice'].tolist()
            close = df['closePrice'].tolist()
            slowk, slowd = talib.STOCH(numpy.array(high), numpy.array(low), numpy.array(close), fastk_period=9,
                                       slowk_period=3,
                                       slowk_matype=0,
                                       slowd_period=3,
                                       slowd_matype=0)

            upper, middle, lower = talib.BBANDS(numpy.array(close),
                                                timeperiod=20, nbdevup=2.1,
                                                nbdevdn=2.1, matype=0)
            df['sma5'] = sma10
            df['sma10'] = sma10
            df['slowk'] = slowk
            df['slowd'] = slowd
            df['upper'] = upper
            df['middle'] = middle
            df['lower'] = lower

            if hitsoryitem['buyDate'] is None:
                mask = (df['date'] > hitsoryitem['createDate'])
            else:
                mask = (df['date'] > hitsoryitem['buyDate'])

            df = df.loc[mask]

            for index, item in df.iterrows():

                if hitsoryitem['buyDate'] is None:
                    priceDiff = item['closePrice'] - item['middle']

                    cp = priceDiff / item['closePrice']

                    if priceDiff > 0 or priceDiff < 0 and cp < 0.03:
                        obs_analysis_tabel.update({"_id": hitsoryitem['_id']},
                                                  {'$set': {'buyDate': item['date'], 'buyPrice': item['closePrice']}})

                        hitsoryitem = obs_analysis_tabel.find_one({"_id": hitsoryitem['_id']})
                        print("buy {0}".format(stockName))
                    if priceDiff < 0 and cp > -0.03:
                        print("delete {0}".format(stockName))
                        obs_analysis_tabel.delete_one({"_id": hitsoryitem['_id']})

                else:
                    wpt_price = item['closePrice'] - hitsoryitem['buyPrice']
                    cost = wpt_price * 1000
                    if item['closePrice'] > 0:
                        WPCT = (wpt_price / item['closePrice']) * 100
                    else:
                        WPCT = 0

                    if WPCT > 3:
                        print('WPCT > 5 {0}-{1} Date:{2} closePrice:{3} sma5:{4} WPCT:{5}'.format(
                            hitsoryitem['stockName'],
                            hitsoryitem['stockNo'],
                            item['date'],
                            item['closePrice'],
                            item['sma5'],
                            WPCT))

                        if item['closePrice'] < item['upper']:
                            print('{0}-{1}'.format(stockName, stockNo))
                            print("buy date {0} - sell date {1}".format(hitsoryitem['buyDate'], item['date']))
                            print("buy price {0}- sell price {1}".format(hitsoryitem['buyPrice'],
                                                                         item['closePrice']))

                            obs_analysis_tabel.update(
                                {"_id": hitsoryitem['_id']},
                                {'$set': {'isSell': True, 'sellDate': item['date'],
                                          'sellPrice': item['closePrice'],
                                          'wpct': WPCT, 'cost': cost}})
                            break
                    elif WPCT < -10:
                        print('{0}-{1}'.format(stockName, stockNo))
                        print("buy date {0} - sell date {1}".format(hitsoryitem['buyDate'], item['date']))
                        print("buy price {0}- sell price {1}".format(hitsoryitem['buyPrice'],
                                                                     item['closePrice']))

                        obs_analysis_tabel.update(
                            {"_id": hitsoryitem['_id']},
                            {'$set': {'isSell': True, 'sellDate': item['date'],
                                      'sellPrice': item['closePrice'],
                                      'wpct': WPCT, 'cost': cost}})
                        break
                    else:
                        if item['closePrice'] < item['sma5']:
                            date1 = item['date'] - timedelta(days=1)
                            date2 = item['date'] - timedelta(days=2)
                            date3 = item['date'] - timedelta(days=3)
                            day1 = find_agents(stockNo, date1.strftime("%Y%m%d"), 20)
                            if float(day1['concentrated']) < 0:
                                day2 = find_agents(stockNo, date2.strftime("%Y%m%d"), 20)
                                if float(day2['concentrated']) < 0:
                                    day3 = find_agents(stockNo, date3.strftime("%Y%m%d"), 20)
                                    if float(day3['concentrated']) < 0:
                                        print('{0}-{1}'.format(stockName, stockNo))
                                        print(
                                            "buy date {0} - sell date {1}".format(hitsoryitem['buyDate'], item['date']))
                                        print("buy price {0}- sell price {1}".format(hitsoryitem['buyPrice'],
                                                                                     item['closePrice']))

                                        obs_analysis_tabel.update({"_id": hitsoryitem['_id']},
                                                                  {'$set': {'isSell': True,
                                                                            'sellDate': item['date'],
                                                                            'sellPrice': item['closePrice'],
                                                                            'wpct': WPCT, 'cost': cost}})
                                        break

                    print('update {0}-{1} Date:{2} closePrice:{3} sma5:{4} WPCT:{5}'.format(hitsoryitem['stockName'],
                                                                                            hitsoryitem['stockNo'],
                                                                                            item['date'],
                                                                                            item['closePrice'],
                                                                                            item['sma5'],
                                                                                            WPCT))
                    obs_analysis_tabel.update({"_id": hitsoryitem['_id']}, {'$set': {'wpct': WPCT}})


def history_analysis(hitsoryitem):
    stockNo = hitsoryitem['stockNo']
    item = collect.find_one({'stockNo': stockNo})
    if item is not None:
        stockName = item['stockName']
        details = item['details']
        if details is not None and len(details) > 0:
            details.sort(key=itemgetter('date'), reverse=False)

            df = pd.DataFrame(list(details))
            df['Date'] = pd.to_datetime(df['date'])
            df.set_index('Date', inplace=True)
            df.sort_index(inplace=True)
            df.head()
            close_list = df.fillna(0).closePrice.tolist()
            sma5 = talib.SMA(numpy.array(close_list), timeperiod=5)
            sma10 = talib.SMA(numpy.array(close_list), timeperiod=10)
            high = df['highPrice'].tolist()
            low = df['lowPrice'].tolist()
            close = df['closePrice'].tolist()
            slowk, slowd = talib.STOCH(numpy.array(high), numpy.array(low), numpy.array(close), fastk_period=9,
                                       slowk_period=3,
                                       slowk_matype=0,
                                       slowd_period=3,
                                       slowd_matype=0)
            df['sma5'] = sma10
            df['sma10'] = sma10
            df['slowk'] = slowk
            df['slowd'] = slowd
            mask = (df['date'] > hitsoryitem['date'])
            df = df.loc[mask]

            for index, item in df.iterrows():
                wpt_price = item['closePrice'] - hitsoryitem['closePrice']
                if item['closePrice'] > 0:
                    WPCT = (wpt_price / item['closePrice']) * 100
                else:
                    WPCT = 0

                if WPCT > 3:
                    print('WPCT > 5 {0}-{1} Date:{2} closePrice:{3} sma5:{4} WPCT:{5}'.format(hitsoryitem['stockName'],
                                                                                              hitsoryitem['stockNo'],
                                                                                              item['date'],
                                                                                              item['closePrice'],
                                                                                              item['sma5'],
                                                                                              WPCT))

                    if item['slowk'] > 90 or item['closePrice'] < item['sma5']:
                        print('{0}-{1}'.format(stockName, stockNo))
                        print("buy date {0} - sell date {1}".format(hitsoryitem['date'], item['date']))
                        print("buy price {0}- sell price {1}".format(hitsoryitem['closePrice'],
                                                                     item['closePrice']))

                        daily_analysis_collect.update({"stockNo": stockNo, 'date': hitsoryitem['date']},
                                                      {'$set': {'isSell': True, 'sellDate': item['date'],
                                                                'sellPrice': item['closePrice'],
                                                                'wpct': WPCT}})
                        break
                elif WPCT < -10:
                    print('{0}-{1}'.format(stockName, stockNo))
                    print("buy date {0} - sell date {1}".format(hitsoryitem['date'], item['date']))
                    print("buy price {0}- sell price {1}".format(hitsoryitem['closePrice'],
                                                                 item['closePrice']))

                    daily_analysis_collect.update({"stockNo": stockNo, 'date': hitsoryitem['date']},
                                                  {'$set': {'isSell': True, 'sellDate': item['date'],
                                                            'sellPrice': item['closePrice'],
                                                            'wpct': WPCT}})
                    break
                else:
                    if item['closePrice'] < item['sma5']:
                        date1 = item['date'] - timedelta(days=1)
                        date2 = item['date'] - timedelta(days=2)
                        date3 = item['date'] - timedelta(days=3)
                        day1 = find_agents(stockNo, date1.strftime("%Y%m%d"), 20)
                        if float(day1['concentrated']) < 0:
                            day2 = find_agents(stockNo, date2.strftime("%Y%m%d"), 20)
                            if float(day2['concentrated']) < 0:
                                day3 = find_agents(stockNo, date3.strftime("%Y%m%d"), 20)
                                if float(day3['concentrated']) < 0:
                                    print('{0}-{1}'.format(stockName, stockNo))
                                    print("buy date {0} - sell date {1}".format(hitsoryitem['date'], item['date']))
                                    print("buy price {0}- sell price {1}".format(hitsoryitem['closePrice'],
                                                                                 item['closePrice']))

                                    daily_analysis_collect.update({"stockNo": stockNo, 'date': hitsoryitem['date']},
                                                                  {'$set': {'isSell': True, 'sellDate': item['date'],
                                                                            'sellPrice': item['closePrice'],
                                                                            'wpct': WPCT}})
                                    break

                print('update {0}-{1} Date:{2} closePrice:{3} sma5:{4} WPCT:{5}'.format(hitsoryitem['stockName'],
                                                                                        hitsoryitem['stockNo'],
                                                                                        item['date'],
                                                                                        item['closePrice'],
                                                                                        item['sma5'],
                                                                                        WPCT))
                daily_analysis_collect.update({"stockNo": stockNo, 'date': hitsoryitem['date']},
                                              {'$set': {'wpct': WPCT}})


def daily_analysis(item, date_str):
    item_count = 120

    stock = item['stockNo']
    name = item['stockName']

    details = item['details']

    if len(details) == 0:
        return
    details.sort(key=itemgetter('date'), reverse=False)

    df = pd.DataFrame(list(details))
    df['Date'] = pd.to_datetime(df['date'])
    df.set_index('Date', inplace=True)
    df.sort_index(inplace=True)
    df.head()

    end = datetime.strptime(date_str + " 23:59:59", '%Y%m%d %H:%M:%S')

    mask = (df['date'] <= end)

    df = df.loc[mask][-item_count:]

    closeList = df.fillna(0).closePrice.tolist()

    if len(closeList) == 0:
        return

    upper, middle, lower = talib.BBANDS(numpy.array(closeList),
                                        timeperiod=20, nbdevup=2.1,
                                        nbdevdn=2.1, matype=0)
    if stock is None:
        return
    up = upper[-1]
    low = lower[-1]
    close = df['closePrice'][-1]

    if not numpy.math.isnan(up):
        if bbandUp(close, up, low, 0.9) and over_60ma(df):
            if bbandzip(df, upper, lower, 20):
                if avgValue(df, date_str, 20) and real_red(df):
                    data20 = find_agents(stock, date_str, 20)
                    if data20 is not None and data20['concentrated'] > 7:
                        data60 = find_agents(stock, date_str, 60)
                        if data60 is not None and data60['concentrated'] > 5:
                            data120 = find_agents(stock, date_str, 120)
                            if data120 is not None and data120['concentrated'] > 0:
                                print("{0}".format(stock))
                                r = collectTWSE.find_one({'stock_id': '{0}'.format(stock)})
                                majorData, ary = find_major(stock, date_str)
                                daily_analysis_collect.insert_one({
                                    'createDate': df['date'][-1],
                                    'stockName': name,
                                    'type': r['type'],
                                    'stockNo': stock,
                                    'closePrice': df['closePrice'][-1],
                                    'openPrice': df['openPrice'][-1],
                                    'highPrice': df['highPrice'][-1],
                                    'lowPrice': df['lowPrice'][-1],
                                    'stockChip': data20['stockChip'],
                                    'total20': data20['total'],
                                    'total60': data60['total'],
                                    'total120': data120['total'],
                                    'avg20': data20['avg'],
                                    'avg60': data60['avg'],
                                    'avg120': data120['avg'],
                                    'concentrated20': data20['concentrated'],
                                    'concentrated60': data60['concentrated'],
                                    'concentrated120': data120['concentrated'],
                                    'majorData': ary,
                                    'sellDate': None,
                                    'sellPrice': 0.0,
                                    'wpct': 0.0,
                                    'isSell': False
                                })


def analysis_bband_lower(item, date_str):
    item_count = 120

    stock = item['stockNo']
    name = item['stockName']

    details = item['details']

    if len(details) == 0:
        return

    details.sort(key=itemgetter('date'), reverse=False)

    df = pd.DataFrame(list(details))
    df['Date'] = pd.to_datetime(df['date'])
    df.set_index('Date', inplace=True)
    df.sort_index(inplace=True)
    df.head()

    end = datetime.strptime(date_str + " 23:59:59", '%Y%m%d %H:%M:%S')

    mask = (df['date'] <= end)

    df = df.loc[mask][-item_count:]

    closeList = df.fillna(0).closePrice.tolist()

    if len(closeList) == 0:
        return

    upper, middle, lower = talib.BBANDS(numpy.array(closeList),
                                        timeperiod=20, nbdevup=2.1,
                                        nbdevdn=2.1, matype=0)
    if stock is None:
        return
    up = upper[-1]
    low = lower[-1]
    close = df['closePrice'][-1]
    lowPrice = df['lowPrice'][-1]
    open = df['openPrice'][-1]
    high = df['highPrice'][-1]

    if not numpy.math.isnan(up):
        if lower_shadows(close_price=close, open_price=open, low_price=lowPrice, high_price=high):
            if bbandLow(close, up, low):

                data60 = find_agents(stock, date_str, 60)
                if data60 is not None and float(data60['concentrated']) > 2:
                    print(stock)
                    data20 = find_agents(stock, date_str, 20)
                    data120 = find_agents(stock, date_str, 120)
                    # if data120 is not None and float(data120['concentrated']) > 0:
                    print("{0}".format(stock))
                    r = collectTWSE.find_one({'stock_id': '{0}'.format(stock)})
                    majorData, ary = find_major(stock, date_str)
                    low_analysis_collect.insert_one({
                        'createDate': df['date'][-1],
                        'stockName': name,
                        'type': r['type'],
                        'stockNo': stock,
                        'closePrice': df['closePrice'][-1],
                        'openPrice': df['openPrice'][-1],
                        'highPrice': df['highPrice'][-1],
                        'lowPrice': df['lowPrice'][-1],
                        'stockChip': data20['stockChip'],
                        'total20': data20['total'],
                        'total60': data60['total'],
                        'total120': data120['total'],
                        'avg20': data20['avg'],
                        'avg60': data60['avg'],
                        'avg120': data120['avg'],
                        'concentrated20': data20['concentrated'],
                        'concentrated60': data60['concentrated'],
                        'concentrated120': data120['concentrated'],
                        'majorData': ary,
                        'sellDate': None,
                        'sellPrice': 0.0,
                        'wpct': 0.0,
                        'isSell': False
                    })


def value_analysis(stockNo):
    items = monthly_value_collect.find({'stockNo': stockNo})
    for item in items:
        scores = 0

        details = item['details']

        if len(details) == 0:
            return

        details.sort(key=itemgetter('date'), reverse=True)

        if details[0]['per'] < 20:
            scores += 3.33
        if details[0]['pbr'] < 1.5:
            scores += 3.33
        if details[0]['cyr'] > 3:
            scores += 3.33

        if scores > 8:
            print("{0} scores : {1}".format(stockNo, scores))





def main():
    if len(sys.argv) > 0:
        if sys.argv[1] == 'value':
            twse = collect.find({})
            for index, item in enumerate(twse):
                value_analysis(item['stockNo'])
        if sys.argv[1] == 'chip':
            twse = collect.find({})
            for index, item in enumerate(twse):
                try:
                    chip_analysis(item, datetime.today().strftime("%Y%m%d"), False)
                except Exception as e:
                    print('{}-{}'.format(item['stockNo'], e))
        elif sys.argv[1] == 'history':
            if sys.argv[2] == 'obs':
                items = obs_analysis_tabel.find({})
                for item in items:
                    history_analysis_chip(item)
            else:
                # reset_history()
                items = daily_analysis_collect.find({})

                items = list(items)
                items.sort(key=itemgetter('date'), reverse=False)
                """
                df = pd.DataFrame(list(items))
                df['Date'] = pd.to_datetime(df['date'])
                df.set_index('Date', inplace=True)
                df.sort_index(inplace=True)
                df.head()
                end = datetime.strptime("20171012 23:59:59", '%Y%m%d %H:%M:%S')
                mask = (df['date'] > end)
                df = df.loc[mask]
                """

                for item in items:
                    if not item['isSell']:
                        history_analysis(item)

        elif sys.argv[1] == 'simulation':
            twse = collect.find({})
            item = twse[0]
            details = item['details']
            details.sort(key=itemgetter('date'), reverse=False)

            df = pd.DataFrame(list(details))
            df['Date'] = pd.to_datetime(df['date'])
            df.set_index('Date', inplace=True)
            df.sort_index(inplace=True)
            df.head()
            end = datetime.strptime("20190101 23:59:59", '%Y%m%d %H:%M:%S')
            mask = (df['date'] > end)
            df = df.loc[mask]

            dateList = df['date'].tolist()

            for da in dateList:
                print(da)
                t = collect.find({})

                for index, item in enumerate(t):
                    if sys.argv[2] == 'obs':
                        try:
                            chip_analysis(item, da.strftime("%Y%m%d"), True)
                        except Exception as e:
                            print('{}-{}'.format(item['stockNo'], e))
                    elif sys.argv[2] == 'low':
                        try:
                            analysis_bband_lower(item, da.strftime("%Y%m%d"))
                        except Exception as e:
                            print(item)
                    else:
                        try:
                            itm = daily_analysis_collect.find_one({'stockNo': item['stockNo']})
                            if itm is not None:
                                continue
                            daily_analysis(item, da.strftime("%Y%m%d"))
                        except Exception as e:
                            print('{}-{}'.format(item['stockNo'], e))

        elif sys.argv[1] == 'low':
            twse = collect.find({})
            for index, item in enumerate(twse):
                try:

                    analysis_bband_lower(item, datetime(2019, 10, 16).strftime("%Y%m%d"))
                except Exception as e:
                    print(item)
        else:
            twse = collect.find({})
            for index, item in enumerate(twse):
                try:
                    daily_analysis(item, datetime(2019, 1, 2).strftime("%Y%m%d"))
                except Exception as e:
                    print('{}-{}'.format(item['stockNo'], e))


if __name__ == '__main__':
    main()
