from operator import attrgetter, itemgetter
from datetime import datetime
import requests
from bs4 import BeautifulSoup
from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client['stock']
collectTWSE = db['twse_list']

headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36"}

twse = collectTWSE.find()
row_count = twse.count()

for index, item in enumerate(twse):
    stock_no = item['stock_id']
    stock_name = item['stock_name']
    print("stock no {0}-{1}".format(stock_no, stock_name))
    url = "http://www.wantgoo.com/stock/astock/basic?stockno={0}".format(stock_no)
    res = requests.post(url, headers=headers)
    soup = BeautifulSoup(res.text, 'html.parser')
    table = soup.find_all('table', attrs={'class': 'tb-vl'})

    if table is None:
        continue
    if len(table) == 0:
        continue
    table_body = table[1].find('tbody')
    rows = table_body.find_all('tr')
    data = []

    for row in rows:

        th = row.find_all('th')

        if len(th) == 2:
            if th[1].text == '已發行普通股數':
                td = row.find_all('td')
                chip = td[1].text.replace(',','')
                print(chip)
                collectTWSE.update({"_id": item['_id']}, {"$set": {"chip": chip}})
                break
