from operator import itemgetter

from datetime import datetime


class Analysis_bband_lower(object):
    item_count = 120

    stock = item['stockNo']
    name = item['stockName']

    details = item['details']

    details.sort(key=itemgetter('date'), reverse=False)

    df = pd.DataFrame(list(details))
    df['Date'] = pd.to_datetime(df['date'])
    df.set_index('Date', inplace=True)
    df.sort_index(inplace=True)
    df.head()

    end = datetime.strptime(date_str + " 23:59:59", '%Y%m%d %H:%M:%S')

    mask = (df['date'] <= end)

    df = df.loc[mask][-item_count:]

    closeList = df.fillna(0).closePrice.tolist()

    upper, middle, lower = talib.BBANDS(numpy.array(closeList),
                                        timeperiod=20, nbdevup=2.1,
                                        nbdevdn=2.1, matype=0)
    up = upper[-1]
    low = lower[-1]
    close = df['closePrice'][-1]
    lowPrice = df['lowPrice'][-1]
    open = df['openPrice'][-1]
    high = df['highPrice'][-1]

    if not numpy.math.isnan(up):
        if lower_shadows(close_price=close, open_price=open, low_price=lowPrice, high_price=high):
            if bbandLow(close, up, low):

                data60 = find_agents(stock, date_str, 60)
                if data60 is not None and float(data60['concentrated']) > 2:
                    data20 = find_agents(stock, date_str, 20)
                    data120 = find_agents(stock, date_str, 120)
                    # if data120 is not None and float(data120['concentrated']) > 0:
                    print("{0}".format(stock))
                    r = collectTWSE.find_one({'stock_id': '{0}'.format(stock)})
                    majorData, ary = find_major(stock, date_str)
                    low_analysis_collect.insert_one({
                        'createDate': df['date'][-1],
                        'stockName': name,
                        'type': r['type'],
                        'stockNo': stock,
                        'closePrice': df['closePrice'][-1],
                        'openPrice': df['openPrice'][-1],
                        'highPrice': df['highPrice'][-1],
                        'lowPrice': df['lowPrice'][-1],
                        'stockChip': data20['stockChip'],
                        'total20': data20['total'],
                        'total60': data60['total'],
                        'total120': data120['total'],
                        'avg20': data20['avg'],
                        'avg60': data60['avg'],
                        'avg120': data120['avg'],
                        'concentrated20': data20['concentrated'],
                        'concentrated60': data60['concentrated'],
                        'concentrated120': data120['concentrated'],
                        'majorData': ary,
                        'sellDate': None,
                        'sellPrice': 0.0,
                        'wpct': 0.0,
                        'isSell': False
                    })


if __name__ == '__main__':
    main()
