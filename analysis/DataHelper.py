import configparser
from pymongo import MongoClient


class DataHelper(object):


    def __init__(self):
        config = configparser.ConfigParser()
        config.optionxform = str
        config.read('config.ini')
        host = config.get('DEFAULT', 'mongodb_host')
        port = config.get('DEFAULT', 'mongodb_port')

        client = MongoClient(host, int(port))
        db = client['stock']
        collect = db['stock']
        major = db['major']
        obs_analysis_tabel = db['obs_analysis']
        chip_db = db['chip']
        real_time = db['real_time']
        agents = db['agents']
        daily_analysis_collect = db['daily_analysis']
        low_analysis_collect = db['low_analysis']
        daily_analysis_collect2 = db['daily_analysis2']
        collectTWSE = db['twse_list']
        collectOTC = db['otc_list']



