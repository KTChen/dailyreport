import json
from _operator import itemgetter
from datetime import datetime
import requests
from pymongo import MongoClient
import pandas as pd

client = MongoClient('127.0.0.1', int('27017'))
db = client['stock']
chip_db = db['chip']
chip_db2 = db['chip2']
chip_daily_db = db['chip_daily']
collectTWSE = db['twse_list']
collectStock = db['stock']

chip_twse = db['chip_twse']
chip_twse_2019_Q4 = db['chip_twse_2019_Q4']
chip_twse_2019_Q3 = db['chip_twse_2019_Q3']
chip_otc = db['chip_otc']
chip_otc_2019_Q4 = db['chip_otc_2019_Q4']
chip_otc_2019_Q3 = db['chip_otc_2019_Q3']

def main():
    items = collectTWSE.find({})
    for index, item in enumerate(items):
        stock_no = item['stock_id']

        print("stock_no {0} - index {1} ".format(stock_no, index))

        stock_type = item['type']

        if index < 1584:
            continue

        cr = chip_db.find_one({'stockNo': stock_no})
        cr2 = chip_db2.find_one({'stockNo': stock_no})
        if cr is None:
            details = []
        else:
            details = cr['details']
        if cr2 is None:
            details2 = []
        else:
            details2 = cr2['details']

        new_data = {'2018Q1': [], '2018Q2': [], '2018Q3': [], '2018Q4': [], '2019Q1': [], '2019Q2': [], '2019Q3': [], '2019Q4': [], '2020Q1': [], '2020Q2': [], '2020Q3': [], '2020Q4': []}

        season = ''

        for data in details:
            stock_date = data['date']

            year = stock_date.year

            if 1 <= stock_date.month <= 3:
                season = 'Q1'
            elif 4 <= stock_date.month <= 6:
                season = 'Q2'
            elif 7 <= stock_date.month <= 9:
                season = 'Q3'
            elif 10 <= stock_date.month <= 12:
                season = 'Q4'

            dicName = "{0}{1}".format(year, season)
            arr = new_data[dicName]

            d = [data for data in arr if
                 data.get('date').strftime("%Y-%m-%d 00:00:00") == stock_date.strftime(
                     "%Y-%m-%d 00:00:00")]
            if len(d) == 0:
                arr.append(data)

        for data in details2:
            stock_date = data['date']

            year = stock_date.year

            if 1 < stock_date.month <= 3:
                season = 'Q1'
            elif 4 < stock_date.month <= 6:
                season = 'Q2'
            elif 7 < stock_date.month <= 9:
                season = 'Q3'
            elif 10 < stock_date.month <= 12:
                season = 'Q4'

            dicName = "{0}{1}".format(year, season)
            arr = new_data[dicName]

            d = [data for data in arr if
                 data.get('date').strftime("%Y-%m-%d 00:00:00") == stock_date.strftime(
                     "%Y-%m-%d 00:00:00")]
            if len(d) == 0:
                arr.append(data)

        for index, key in enumerate(new_data):
            items = new_data[key]
            items.sort(key=itemgetter('date'), reverse=True)

            if len(items) == 0:
                continue

            stock_data = {
                'stockNo': stock_no,
                'details': items
            }

            chip_twse = db["chip_twse_{0}".format(key, season)]

            chip_twse.insert_one(stock_data)


if __name__ == '__main__':
    main()