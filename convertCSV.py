from datetime import datetime, timedelta
from operator import itemgetter

import decimal
from pymongo import MongoClient
import sys
import talib
import pandas as pd
import configparser
import numpy

config = configparser.ConfigParser()
config.optionxform = str
config.read('config.ini')
host = config.get('DEFAULT', 'mongodb_host')
port = config.get('DEFAULT', 'mongodb_port')

client = MongoClient(host, int(port))
db = client['stock']
collect = db['stock']
major = db['major']
obs_analysis_tabel = db['obs_analysis']
chip_db = db['chip']
real_time = db['real_time']
agents = db['agents']
daily_analysis_collect = db['daily_analysis']
daily_analysis_collect2 = db['daily_analysis2']
collectTWSE = db['twse_list']
collectOTC = db['otc_list']

items = obs_analysis_tabel.find()

# items.sort(key=itemgetter('createDate'), reverse=False)
df = pd.DataFrame(list(items))
df['buyDate'] = pd.to_datetime(df['buyDate'])
df.set_index('buyDate', inplace=True)
df.sort_index(inplace=True)
df.head()
df.to_csv('obs_analysis_tabel.csv')

buyList = df['createDate']
start = buyList[0]
diff = start - datetime.today()

days = range(-diff.days)
totalPrice = 0
totalList = []

dateList = dict()


for index, d in df.iterrows():
    dateList.update({index: index})

for bd in dateList.keys():

    for index, dd in df.iterrows():

        if bd == index:
            totalPrice += dd['buyPrice']
            totalList.append((bd, totalPrice * 1000))
            print("日期：{0} 買進 {1} 持有：{2}".format(bd, '%.0f' % (dd['buyPrice'] * 1000), '%.0f' % (totalPrice * 1000)))
        if bd == dd['sellDate']:
            totalPrice -= dd['sellPrice']
            print("日期：{0} 賣出 {1} 持有：{2}".format(bd, '%.0f' % (dd['sellPrice'] * 1000), '%.0f' % (totalPrice * 1000)))
            totalList.append((bd, totalPrice * 1000))


df.to_csv('obs_analysis_tabel.csv', sep='\t', encoding='utf-8')

l = []
for t in totalList:
    l.append(t[1])

print('Max : {0}'.format('%.0f' % max(l)))
