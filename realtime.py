#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
import json
import time
import requests
import configparser
import logging

import sys
from pymongo import MongoClient
from linebot import LineBotApi
from linebot.models import TextSendMessage
from analysis import Analysis

client = MongoClient('127.0.0.1', 27017)
db = client['stock']
user = db['user']

config = configparser.ConfigParser()
config.optionxform = str
config.read('config.ini')
line_token = config.get('DEFAULT', 'line_token')

targets = []
targetStocks = []
source = {}
log_path = 'log.txt'

logging.basicConfig(filename=log_path)


class RealTime(object):
    def __init__(self, targets, max_stock_per_crawler=50):
        self.crawlers = []

        for index in range(0, len(targets), max_stock_per_crawler):
            crawler = Crawler(targets[index:index + max_stock_per_crawler])
            self.crawlers.append(crawler)

    def run(self):
        data = []
        for crawler in self.crawlers:
            data.extend(crawler.get_data())
        return data


class Crawler(object):
    '''Request to Market Information System'''

    def __init__(self, targets):
        self.targets = targets
        # Add 1000 seconds for prevent time inaccuracy

    def get_data(self):
        data = []
        endpoint = 'http://mis.twse.com.tw/stock/api/getStockInfo.jsp'
        timestamp = int(time.time() * 1000 + 1000000)
        channels = '%7c'.join('{}'.format(target) for target in self.targets)
        query_url = '{}?ex_ch={}&json=1&delay=0&_={}'.format(endpoint, channels, timestamp)
        try:
            # Get original page to get session
            req = requests.session()
            req.get('http://mis.twse.com.tw/stock/index.jsp', headers={'Accept-Language': 'zh-TW'})
            response = req.get(query_url)

            text = response.text.encode('ascii', 'ignore').decode('ascii')

            content = json.loads(text)
        except Exception as err:
            print(err)
        else:
            if 'msgArray' in content:
                data = content['msgArray']
            else:
                time.sleep(5)

        return data


class Recorder(object):
    def record_to_csv(self, data, source):
        for row in data:

            print(row)

            a = str(row['a']).split('_')[:-1]
            f = str(row['f']).split('_')[:-1]
            b = str(row['b']).split('_')[:-1]
            g = str(row['g']).split('_')[:-1]

            if len(a) == 0:
                a.append("0")
                a.append("0")
                a.append("0")
                a.append("0")
                a.append("0")

            if len(f) == 0:
                f.append("0")
                f.append("0")
                f.append("0")
                f.append("0")
                f.append("0")

            if len(b) == 0:
                b.append("0")
                b.append("0")
                b.append("0")
                b.append("0")
                b.append("0")

            if len(g) == 0:
                g.append("0")
                g.append("0")
                g.append("0")
                g.append("0")
                g.append("0")

            """
             print('買進               賣出 '.format(b[0], g[0], a[0], f[0]))
            print('{0} : {1}         {2} : {3}'.format(b[0], g[0], a[0], f[0]))
            print('{0} : {1}         {2} : {3}'.format(b[1], g[1], a[1], f[1]))
            print('{0} : {1}         {2} : {3}'.format(b[2], g[2], a[2], f[2]))
            print('{0} : {1}         {2} : {3}'.format(b[3], g[3], a[3], f[3]))
            print('{0} : {1}         {2} : {3}'.format(b[4], g[4], a[4], f[4]))

            """

            up = source[row['c']]
            price = float(row['z'])
            # 漲2%
            diff = (price * 0.015)

            if price > up + diff:
                users = user.find({})
                real_time = db['real_time']
                item = real_time.find_one({'stockNo': row['c']})
                targetStocks.remove(item['type'])
                string = '{0} 價格 ： {1} 突破'.format(row['c'], price)

                for usr in users:
                    line_bot_api = LineBotApi(line_token)
                    text_message = TextSendMessage(string)
                    line_bot_api.push_message(usr['user'], text_message)


def main():
    real_time = db['real_time']
    items = real_time.find()

    for index, item in enumerate(items):
        targets.append(item['stockNo'])
        targetStocks.append(item['type'])

    analysis = Analysis(targets)
    source = analysis.find()

    print("process start at : {0}".format(datetime.today()))
    while len(targetStocks) > 0:
        try:
            if datetime.today().time().hour == 13 and datetime.today().time().minute == 30:
                print("process exit at : {0}".format(datetime.today()))
                sys.exit(0)

            real_time = RealTime(targetStocks)
            data = real_time.run()
            r = Recorder()
            r.record_to_csv(data, source)
            time.sleep(5)
        except Exception as e:
            logging.warning(e)


if __name__ == '__main__':
    main()
