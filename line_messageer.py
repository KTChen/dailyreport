#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import configparser

from datetime import datetime
from linebot import LineBotApi
from linebot.models import TextSendMessage, TemplateSendMessage, \
    CarouselTemplate, CarouselColumn, PostbackTemplateAction, URITemplateAction
from flask import Flask, request, json
from pymongo import MongoClient
from analysis import Analysis
from chart import Chart

app = Flask(__name__)
config = configparser.ConfigParser()
config.optionxform = str
config.read('config.ini')
host = config.get('DEFAULT', 'mongodb_host')
port = config.get('DEFAULT', 'mongodb_port')
ngrok_host = config.get('DEFAULT', 'ngrok_host')
line_token = config.get('DEFAULT', 'line_token')

client = MongoClient(host, int(port))
db = client['stock']
real_time = db['real_time']
user = db['user']
daily_analysis_collect = db['daily_analysis']
line_bot_api = LineBotApi(line_token)


@app.route('/')
def index():
    return "<p>Hello World!</p>"


@app.route('/<path:path>')
def static_file(path):
    return app.send_static_file(path)


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def create_actions(stockNo, image_url):
    actions = [
        PostbackTemplateAction(
            label='籌碼',
            data='/chip {}'.format(stockNo)
        ),
        PostbackTemplateAction(
            label='大戶籌碼',
            data='/major {}'.format(stockNo)
        ),
        URITemplateAction(
            label='K線',
            uri='http://61.70.176.56:8000/getStock/?stockNo={}'.format(stockNo)
        )
    ]
    return actions


def create_column(stockNo, stockName, data, image_url):
    column = CarouselColumn(
        thumbnail_image_url=image_url,
        title='{0}-{1}'.format(stockNo, stockName),
        text=data,
        actions=create_actions(stockNo, image_url)
    )
    return column


def send_message(source, source_type, message):
    if source_type == 'user':
        line_bot_api.push_message(source['userId'], message)
    else:
        line_bot_api.push_message(source['groupId'], message)


@app.route('/callback', methods=['POST'])
def callback():
    json_line = request.get_json()
    json_line = json.dumps(json_line)
    decoded = json.loads(json_line)

    event = decoded['events'][0]
    message = ''
    if 'postback' in event:
        message = event['postback']['data']
    elif 'message' in event:
        message = event['message']['text']

    source = decoded['events'][0]['source']
    source_type = decoded['events'][0]['source']['type']

    if message.lower() == '/obs':

        analysis = Analysis()
        items = analysis.get_obs()
        items = chunks(items, 10)
        chart = Chart()
        for ary in items:
            columns = []
            for date, item in ary.iterrows():
                stockNo = item['stockNo']
                name = item['stockName']
                image_url = 'https://{0}/static/{1}/{2}'.format(ngrok_host, stockNo,
                                                                datetime.today().strftime("%Y%m%d") + '/stock.png')
                path = 'static/{0}/{1}'.format(stockNo, datetime.today().strftime("%Y%m%d"))
                if not os.path.exists(path):
                    chart.getData(stockNo)
                data = analysis.analysis_less_chip(stockNo)
                columns.append(create_column(stockNo=stockNo, stockName=name, data=data, image_url=image_url))

            carousel_template_message = TemplateSendMessage(
                alt_text='盤中觀察股',
                template=CarouselTemplate(
                    columns=columns
                )
            )

            send_message(source=source, source_type=source_type, message=carousel_template_message)

    if message.lower() == '/daily':
        analysis = Analysis()
        items = analysis.get_daily()
        items = chunks(items, 10)
        chart = Chart()
        for ary in items:
            columns = []
            for date, item in ary.iterrows():

                stockNo = item['stockNo']
                name = item['stockName']
                image_url = 'https://{0}/static/{1}/{2}'.format(ngrok_host, stockNo,
                                                                datetime.today().strftime("%Y%m%d") + '/stock.png')

                path = 'static/{0}/{1}'.format(stockNo, datetime.today().strftime("%Y%m%d"))
                if not os.path.exists(path):
                    chart.getData(stockNo)

                data = analysis.analysis_less_chip(stockNo)
                columns.append(create_column(stockNo=stockNo, stockName=name, data=data, image_url=image_url))

            message = TemplateSendMessage(
                alt_text='本日程式選股',
                template=CarouselTemplate(
                    columns=columns
                )
            )
            send_message(source=source, source_type=source_type, message=message)


    if '/help' in message.lower():
        text_message = TextSendMessage("/chip 2330\r\n(查詢個股本日籌碼)\r\n\r\n"
                                       "/major 2330\r\n(查詢個股大戶買賣超)\r\n\r\n"
                                       "I2330\r\n(查詢個股概況)\r\n\r\n"
                                       "/regression 2330 20171001\r\n(查詢指定日期)\r\n\r\n"
                                       "/daily\r\n(今日盤後分析標的)\r\n\r\n"
                                       "/obs\r\n(明日盤中觀察標的)\r\n\r\n"
                                       "/history\r\n(歷史資料)")
        send_message(source=source, source_type=source_type, message=text_message)

    if '/history' in message.lower():
        text_message = TextSendMessage("http://61.70.176.56:8000/getHistory")
        send_message(source=source, source_type=source_type, message=text_message)

    if '/chip' in message.lower():
        ary = message.split(' ')

        stock = ary[1]
        analysis = Analysis()
        data = analysis.analysis_stock(stock)
        text_message = TextSendMessage(data)
        send_message(source=source, source_type=source_type, message=text_message)

    if '/major' in message.lower():
        ary = message.split(' ')
        stock = ary[1]
        analysis = Analysis()
        data = analysis.analysis_major(stock)
        if data is None:
            text_message = TextSendMessage('{0} major 查無資料'.format(stock))
        else:
            text_message = TextSendMessage(data)

        send_message(source=source, source_type=source_type, message=text_message)

    if message[:1].lower() == 'i':

        stockNo = message[1:5]
        chart = Chart()
        image_url = 'https://{0}/static/{1}/{2}'.format(ngrok_host, stockNo,
                                                        datetime.today().strftime("%Y%m%d") + '/stock.png')
        path = 'static/{0}/{1}'.format(stockNo, datetime.today().strftime("%Y%m%d"))
        if not os.path.exists(path):
            chart.getData(stockNo)
        analysis = Analysis()
        name, stock, r, data = analysis.analysis_chip(stockNo)
        carousel_template_message = TemplateSendMessage(
            alt_text='Carousel template',
            template=CarouselTemplate(
                columns=[create_column(stockNo=stockNo,
                                       stockName=name,
                                       data=data,
                                       image_url=image_url)

                         ]
            )
        )
        send_message(source=source, source_type=source_type, message=carousel_template_message)

    if '/regression' in message.lower():
        ary = message.split(' ')
        stock = ary[1]
        date = ary[2]
        analysis = Analysis()
        data = analysis.analysis_stock(stock, date)
        text_message = TextSendMessage(data)
        send_message(source=source, source_type=source_type, message=text_message)

    if message.lower() == '/register':
        text_message = TextSendMessage("註冊成功，將會收到盤中監控訊息")
        send_message(source=source, source_type=source_type, message=text_message)
        if source_type == 'user':
            user.insert_one({'user': source['userId'], 'type': source_type, 'createDate': datetime.today()})
        else:
            user.insert_one({'user': source['groupId'], 'type': source_type, 'createDate': datetime.today()})


    return ''


if __name__ == '__main__':
    app.run(debug=False)
