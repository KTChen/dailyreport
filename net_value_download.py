import json
from _operator import itemgetter
from datetime import datetime, timedelta
import requests
import sys

from grs import TWSEOpen
from pymongo import MongoClient
import pandas as pd
import configparser


config = configparser.ConfigParser()
config.optionxform = str
config.read('config.ini')
host = config.get('DEFAULT', 'mongodb_host')
port = config.get('DEFAULT', 'mongodb_port')
client = MongoClient(host, int(port))
db = client['stock']
chip_db = db['chip']
chip_daily_db = db['chip_daily']
collectTWSE = db['twse_list']


