from operator import itemgetter
from pymongo import MongoClient
from datetime import datetime
from bs4 import BeautifulSoup
import requests
import configparser

config = configparser.ConfigParser()
config.optionxform = str
config.read('config.ini')
host = config.get('DEFAULT', 'mongodb_host')
port = config.get('DEFAULT', 'mongodb_port')
user_agent = config.get('DEFAULT', 'user_agent')
monthly_revenue_url = config.get('DEFAULT', 'monthly_revenue_url')
client = MongoClient(host, int(port))
db = client['stock']
twse_collect = db['twse_list']
monthly_revenue_collect = db['monthly_revenue']
headers = {"User-Agent": user_agent}


def download_monthly_revenue(stock_no, stock_name):
    url = monthly_revenue_url.format(stock_no)
    result = requests.get(url)
    soup = BeautifulSoup(result.text, 'html.parser')
    print("stock no {0}".format(stock_no))
    table = soup.find('table', attrs={'class': 'tb rw5n tbhl'})
    if table is None:
        return
    table_body = table.find('tbody')
    rows = table_body.find_all('tr')
    data = []

    json_set_array = []

    for row in rows:

        cols = row.find_all('td')
        cols = [ele.text.strip() for ele in cols]

        try:
            date = cols[0]
            if date == '':
                date = datetime.today()
            else:
                date = datetime.strptime(date, '%Y/%m')
        except IndexError:
            date = ""

        try:
            revenue_current_monthly = cols[1]
            if revenue_current_monthly == '':
                revenue_current_monthly = "0"
        except IndexError:
            revenue_current_monthly = "0"

        try:
            revenue_last_month_percentage = cols[2]
            if revenue_last_month_percentage == '':
                revenue_last_month_percentage = "0"
        except IndexError:
            revenue_last_month_percentage = "0"

        try:
            revenue_last_year = cols[3]
            if revenue_last_year == '':
                revenue_last_year = "0"
        except IndexError:
            revenue_last_year = "0"

        try:
            revenue_last_year_percentage = cols[4]
            if revenue_last_year_percentage == '':
                revenue_last_year_percentage = "0"
        except IndexError:
            revenue_last_year_percentage = "0"

        try:
            revenue_monthly_total = cols[5]
            if revenue_monthly_total == '':
                revenue_monthly_total = "0"
        except IndexError:
            revenue_monthly_total = "0"

        try:
            revenue_last_year_total = cols[6]
            if revenue_last_year_total == '':
                revenue_last_year_total = "0"
        except IndexError:
            revenue_last_year_total = "0"

        try:
            revenue_eq_last_monthly = cols[7]
            if revenue_eq_last_monthly == '':
                revenue_eq_last_monthly = "0"
        except IndexError:
            revenue_eq_last_monthly = "0"

        json_set = {'date': date,
                    'revenue_current_monthly': float(revenue_current_monthly.replace(",", "")),
                    'revenue_last_month_percentage': float(revenue_last_month_percentage.replace(",", "")),
                    'revenue_last_year': float(revenue_last_year.replace(",", "")),
                    'revenue_monthly_total': float(revenue_monthly_total.replace(",", "")),
                    'revenue_last_year_percentage': float(revenue_last_year_percentage.replace(",", "")),
                    'revenue_last_year_total': float(revenue_last_year_total.replace(",", "")),
                    'revenue_eq_last_monthly': float(revenue_eq_last_monthly.replace(",", ""))
                    }
        json_set_array.append(json_set)

    item = monthly_revenue_collect.find_one({"stockNo": stock_no})

    if item is None:
        stock_data = {
            'stockNo': stock_no,
            'stockName': stock_name,
            'details': json_set_array
        }
        monthly_revenue_collect.insert_one(stock_data)
    else:
        details = item['details']
        details.sort(key=itemgetter('date'), reverse=True)
        db_date = details[0]['date']
        insert_date = json_set_array[0]['date']
        if db_date != insert_date:
            monthly_revenue_collect.update({"stockNo": stock_no}, {'$push': {'details': json_set_array[0]}})
        else:
            print("Data exist")


def main():
    twse = twse_collect.find()
    row_count = twse.count()

    for index, item in enumerate(twse):
        stock_no = item['stock_id']
        stock_name = item['stock_name']
        download_monthly_revenue(stock_no, stock_name)


if __name__ == '__main__':
    main()
