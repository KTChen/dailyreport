from operator import itemgetter
from pymongo import MongoClient
from datetime import datetime
from bs4 import BeautifulSoup
import requests
import configparser

config = configparser.ConfigParser()
config.optionxform = str
config.read('config.ini')
host = config.get('DEFAULT', 'mongodb_host')
port = config.get('DEFAULT', 'mongodb_port')
user_agent = config.get('DEFAULT', 'user_agent')
monthly_value_url = config.get('DEFAULT', 'monthly_value_url')
client = MongoClient(host, int(port))
db = client['stock']
twse_collect = db['twse_list']
monthly_value_collect = db['monthly_value']
headers = {"User-Agent": user_agent}


def download_monthly_value(stock_no, stock_name):

    url = monthly_value_url.format(1, stock_no)
    result = requests.get(url)
    soup = BeautifulSoup(result.text, 'html.parser')
    per_table = soup.find('table', attrs={'class': 'tb rw4n tbhl'})
    if per_table is None:
        return
    per_rows = per_table.find('tbody').find_all('tr')

    url = monthly_value_url.format(2, stock_no)
    result = requests.get(url)
    soup = BeautifulSoup(result.text, 'html.parser')
    pbr_table = soup.find('table', attrs={'class': 'tb rw4n tbhl'})
    if pbr_table is None:
        return
    pbr_rows = pbr_table.find('tbody').find_all('tr')

    url = monthly_value_url.format(3, stock_no)
    result = requests.get(url)
    soup = BeautifulSoup(result.text, 'html.parser')
    cyr_table = soup.find('table', attrs={'class': 'tb rw4n tbhl'})
    if cyr_table is None:
        return
    cyr_rows = cyr_table.find('tbody').find_all('tr')

    json_set_array = []

    for index, row in enumerate(per_rows):

        per_cols = per_rows[index].find_all('td')
        per_cols = [ele.text.strip() for ele in per_cols]

        pbr_cols = pbr_rows[index].find_all('td')
        pbr_cols = [ele.text.strip() for ele in pbr_cols]

        cyr_cols = cyr_rows[index].find_all('td')
        cyr_cols = [ele.text.strip() for ele in cyr_cols]

        try:
            date = per_cols[0]
            if date == '':
                date = datetime.today()
            else:
                date = datetime.strptime(date, '%Y/%m')
        except IndexError:
            date = ""

        try:
            per = per_cols[1]
            if per == '' or per == '-':
                per = "0"
        except IndexError:
            per = "0"

        try:
            pbr = pbr_cols[1]
            if pbr == '' or per == '-':
                pbr = "0"
        except IndexError:
            pbr = "0"

        try:
            cyr = cyr_cols[1]
            if cyr == '' or per == '-':
                cyr = "0"
        except IndexError:
            cyr = "0"

        json_set = {'date': date,
                    'per': float(per),
                    'pbr': float(pbr),
                    'cyr': float(cyr)
                    }
        json_set_array.append(json_set)

    item = monthly_value_collect.find_one({"stockNo": stock_no})

    if item is None:
        stock_data = {
            'stockNo': stock_no,
            'stockName': stock_name,
            'details': json_set_array
        }
        monthly_value_collect.insert_one(stock_data)
        print("insert {0}".format(stock_no))
    else:
        details = item['details']
        details.sort(key=itemgetter('date'), reverse=True)
        db_date = details[0]['date']
        insert_date = json_set_array[0]['date']
        if db_date != insert_date:
            monthly_value_collect.update({"stockNo": stock_no}, {'$push': {'details': json_set_array[0]}})
            print("insert {0}".format(stock_no))
        else:
            print("Data exist")


def main():
    twse = twse_collect.find()
    for index, item in enumerate(twse):
        stock_no = item['stock_id']
        stock_name = item['stock_name']
        download_monthly_value(stock_no, stock_name)


if __name__ == '__main__':
    main()
