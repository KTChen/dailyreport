import requests
from pymongo import MongoClient
from bs4 import BeautifulSoup

client = MongoClient('localhost', 27017)
db = client['stock']
collectTWSE = db['twse_list']
collectOTC = db['otc_list']
headers = {
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"}


def twse():
    res = requests.get(
        'http://isin.twse.com.tw/isin/class_main.jsp?owncode=&stockname=&isincode=&market=1&issuetype=1&industry_code=&Page=1&chklike=Y')
    data = res.content
    _de = data.decode('cp950', 'ignore')
    soup = BeautifulSoup(_de, "html5lib")

    stock_no = [tr.findAll('td')[2].text for tr in soup.findAll('tr')]
    stock_name = [tr.findAll('td')[3].text for tr in soup.findAll('tr')]
    category = [tr.findAll('td')[6].text for tr in soup.findAll('tr')]

    for index, item in enumerate(stock_no):
        if index > 0:
            chip = download_stock_info(item, stock_name[index])

            data = {'stock_id': item, 'stock_name': stock_name[index], 'stock_category': category[index],
                    'chip': chip, 'type': 'twse'}
            collectTWSE.insert_one(data)


def otc():
    res = requests.get(
        'http://isin.twse.com.tw/isin/class_main.jsp?owncode=&stockname=&isincode=&market=2&issuetype=4&industry_code=&Page=1&chklike=Y')

    data = res.content
    _de = data.decode('cp950', 'ignore')
    soup = BeautifulSoup(_de, "html5lib")

    stock_no = [tr.findAll('td')[2].text for tr in soup.findAll('tr')]
    stock_name = [tr.findAll('td')[3].text for tr in soup.findAll('tr')]
    category = [tr.findAll('td')[6].text for tr in soup.findAll('tr')]

    for index, item in enumerate(stock_no):
        if index > 0:
            chip = download_stock_info(item, stock_name[index])

            data = {'stock_id': item, 'stock_name': stock_name[index], 'stock_category': category[index], 'chip': chip,
                    'type': 'otc'}
            collectTWSE.insert_one(data)


def download_stock_info(stock_no, stock_name):
    print("stock no {0}".format(stock_no, stock_name))
    url = "http://www.wantgoo.com/stock/astock/basic?stockno={0}".format(stock_no)

    cookies = {
        'cf_clearance': '352e61ff9574f3478ae1583b789d4de59e18c04c-1590550104-0-150',
        'BID': '0C26516D-647C-4CAF-92B0-7075BF595D28',
        '__cfduid': 'd3b68daf6aca592d4c59cbd6033c40e321590550099'
    }

    res = requests.post(url, headers=headers, cookies=cookies)
    soup = BeautifulSoup(res.text, 'html.parser')
    table = soup.find_all('table', attrs={'class': 'tb-vl'})

    if table is None:
        return 0
    if len(table) == 0:
        return 0

    chip = 0

    table_body = table[1].find('tbody')
    rows = table_body.find_all('tr')

    for row in rows:

        th = row.find_all('th')

        if len(th) == 2:
            if th[1].text == '已發行普通股數':
                td = row.find_all('td')
                chip = td[1].text.replace(',', '')
                print(chip)
                break
    return chip


twse()
otc()
