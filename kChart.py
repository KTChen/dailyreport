import matplotlib
import matplotlib.pyplot as plt
import pandas_datareader.data as web
from datetime import datetime
import matplotlib.dates as mdates
from matplotlib.finance import candlestick_ohlc

mondays = mdates.WeekdayLocator(mdates.MONDAY)  # 主要刻度
alldays = mdates.DayLocator()  # 次要刻度
mondayFormatter = mdates.DateFormatter('%m-%d-%Y')  # 如：2-29-2015
dayFormatter = mdates.DateFormatter('%d')  # 如：12

start = datetime.today()
end = datetime.today()
# 台灣股市的話要用 股票代號 加上 .TW

df = web.DataReader('3443.TW', 'yahoo', start, end)
df['Date'] = df.index
df["Date"] = df["Date"].apply(mdates.date2num)

fig, ax = plt.subplots()
fig.subplots_adjust(bottom=0.2)
plt.rcParams['font.sans-serif'] = ['SimHei']  # 用来正常显示中文标签
plt.rcParams['axes.unicode_minus'] = False  # 用来正常显示负号
ax.xaxis.set_major_locator(mondays)
ax.xaxis.set_minor_locator(alldays)
ax.xaxis.set_major_formatter(mondayFormatter)
# ax.xaxis.set_minor_formatter(dayFormatter)


# plot_day_summary(ax, quotes, ticksize=3)
candlestick_ohlc(ax, df[['Date','Open', 'High', 'Low', 'Close', 'Volume']].values, width=0.6, colorup='g',
                 colordown='r',
                 alpha=1.0)

ax.xaxis_date()
ax.autoscale_view()
plt.setp(plt.gca().get_xticklabels(), rotation=45, horizontalalignment='right')

ax.grid(True)
plt.title('3443.TW')
#plt.show()
plt.savefig('static/foo.png')
