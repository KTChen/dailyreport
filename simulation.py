#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from operator import itemgetter
from pymongo import MongoClient
import sys
import talib
import pandas as pd
import configparser
import numpy

config = configparser.ConfigParser()
config.optionxform = str
config.read('config.ini')
host = config.get('DEFAULT', 'mongodb_host')
port = config.get('DEFAULT', 'mongodb_port')

client = MongoClient(host, int(port))
db = client['stock']
collect = db['stock']
major = db['major']
obs_analysis_tabel = db['obs_analysis']
chip_db = db['chip']
real_time = db['real_time']
agents = db['agents']
daily_analysis_collect = db['daily_analysis']
daily_analysis_collect2 = db['daily_analysis2']
collectTWSE = db['twse_list']
collectOTC = db['otc_list']


def find_agents(stockNo, date, delta):
    """
    籌碼買超
    :param agents:
    :param stockNo:
    :param date:
    :return:
    """

    stock = collectTWSE.find_one(
        {'stock_id': stockNo})

    if stock is None:
        return 0, 0, 0, 0, 0

    stockChip = 0
    if 'chip' in stock:
        stockChip = int(stock['chip']) / 1000

    end = datetime.strptime(date + " 23:59:59", '%Y%m%d %H:%M:%S')

    info_result = collect.find_one({'stockNo': stockNo})
    result = chip_db.find_one({'stockNo': stockNo})

    if result is None or info_result is None:
        return 0, 0, 0, 0, 0

    info_details = info_result['details']
    details = result['details']

    info_details.sort(key=itemgetter('date'), reverse=True)
    details.sort(key=itemgetter('date'), reverse=True)

    info_detailsDataFrame = pd.DataFrame(list(info_details))

    detailsDataFrame = pd.DataFrame(list(details))

    info_mask = (info_detailsDataFrame['date'] < end)

    mask = (detailsDataFrame['date'] < end)

    detailsDataFrame = detailsDataFrame.loc[mask][:delta]
    info_detailsDataFrame = info_detailsDataFrame.loc[info_mask][:delta]
    df_array = []

    chips = detailsDataFrame['chips'].tolist()

    for detail in chips:
        for chip in detail:
            df_array.append(chip)

    if len(df_array) == 0:
        return 0, 0, 0, 0, 0

    df = pd.DataFrame(list(df_array))

    groupby = df.groupby('store').agg(
        {'avg': 'mean', 'buySell': 'sum', 'buyValue': 'sum', 'sellValue': 'sum'}).sort_values(['buySell'],
                                                                                              ascending=[False])

    # 前15買超
    groupBuy = groupby[0:15]
    # 前15賣超
    groupSell = groupby[-15:]
    buyStore = groupby[0:3]
    sellStore = groupby[-3:].sort_values(['buySell'], ascending=[True])

    buy = groupBuy['buySell'].sum()
    sell = groupSell['buySell'].sum()
    avg = groupby['avg'].mean()

    data = {}
    totalValues = info_detailsDataFrame['dealValue'].sum() / 1000

    bs = (buy + sell)
    concentrated = (bs / totalValues) * 100

    if stockChip == 0:
        return 0, 0, 0, 0, 0
    else:
        data['stockChip'] = stockChip
        data['total'] = (bs / stockChip) * 100
        data['avg'] = avg
        data['buyStore'] = buyStore
        data['sellStore'] = sellStore
        data['buy'] = buy
        data['sell'] = sell
        data['concentrated'] = concentrated
        return data


def over_60ma(df, timeperiod=60):
    items = df['closePrice'].tolist()

    sma = talib.SMA(numpy.array(items), timeperiod=timeperiod)

    return items[-1] > sma[-1]


def bbandzip(df, upper, lower, timeperiod):
    """
    布林壓縮
    :param details:
    :param upper:
    :param lower:
    :param index:
    :param timeperiod: 天數
    :return:
    """
    bzip = True

    itm = df[-1:]

    sma5 = talib.SMA(numpy.array(df['closePrice'].tolist()), timeperiod=5)

    sma10 = talib.SMA(numpy.array(df['closePrice'].tolist()), timeperiod=10)

    sma20 = talib.SMA(numpy.array(df['closePrice'].tolist()), timeperiod=20)

    a = numpy.array([sma5[-5:],
                     sma10[-5:],
                     sma20[-5:]])
    h = numpy.max(a)
    l = numpy.min(a)
    sideway = 100 * (h - l) / l

    close_items = itm['closePrice'].tolist()
    high_items = itm['highPrice'].tolist()
    upper = upper[-timeperiod:-1]
    lower = lower[-timeperiod:-1]

    for index, close in enumerate(close_items):

        up = upper[index]
        if close > up:
            bzip = False
            break
        else:
            continue

    for index, high in enumerate(high_items):

        up = upper[index]
        if high > up:
            bzip = False
            break
        else:
            continue

    if sideway < 0.5:
        bzip = False

    return bzip


def history_analysis_chip(hitsoryitem):
    stockNo =  hitsoryitem['stockNo']
    item = collect.find_one({'stockNo': stockNo})

    if item is not None:
        stockName = item['stockName']
        details = item['details']
        if details is not None and len(details) > 0:
            details.sort(key=itemgetter('date'), reverse=False)
            df = pd.DataFrame(list(details))
            df['Date'] = pd.to_datetime(df['date'])
            df.set_index('Date', inplace=True)
            df.sort_index(inplace=True)
            df.head()
            close_list = df.fillna(0).closePrice.tolist()
            sma10 = talib.SMA(numpy.array(close_list), timeperiod=10)
            high = df['highPrice'].tolist()
            low = df['lowPrice'].tolist()
            close = df['closePrice'].tolist()
            slowk, slowd = talib.STOCH(numpy.array(high), numpy.array(low), numpy.array(close), fastk_period=9,
                                       slowk_period=3,
                                       slowk_matype=0,
                                       slowd_period=3,
                                       slowd_matype=0)

            upper, middle, lower = talib.BBANDS(numpy.array(close),
                                                timeperiod=20, nbdevup=2.1,
                                                nbdevdn=2.1, matype=0)
            df['sma5'] = sma10
            df['sma10'] = sma10
            df['slowk'] = slowk
            df['slowd'] = slowd
            df['upper'] = upper
            df['middle'] = middle
            df['lower'] = lower

            if hitsoryitem['buyDate'] is None:
                mask = (df['date'] > hitsoryitem['createDate'])
            else:
                mask = (df['date'] > hitsoryitem['buyDate'])

            df = df.loc[mask]

            for index, item in df.iterrows():

                if hitsoryitem['buyDate'] is None:
                    priceDiff = item['closePrice'] - item['middle']

                    cp = priceDiff / item['closePrice']

                    if priceDiff > 0 or priceDiff < 0 and cp < 0.03:
                        obs_analysis_tabel.update({"_id": hitsoryitem['_id']},
                                                  {'$set': {'buyDate': item['date'], 'buyPrice': item['closePrice']}})

                        hitsoryitem = obs_analysis_tabel.find_one({"_id": hitsoryitem['_id']})
                        print("buy {0}".format(stockName))
                    if priceDiff < 0 and cp > -0.03:
                        print("delete {0}".format(stockName))
                        #obs_analysis_tabel.delete_one({"_id": hitsoryitem['_id']})

                else:
                    wpt_price = item['closePrice'] - hitsoryitem['buyPrice']
                    cost = wpt_price * 1000
                    if item['closePrice'] > 0:
                        WPCT = (wpt_price / item['closePrice']) * 100
                    else:
                        WPCT = 0

                    if WPCT > 3:
                        print('WPCT > 5 {0}-{1} Date:{2} closePrice:{3} sma5:{4} WPCT:{5}'.format(
                            hitsoryitem['stockName'],
                            hitsoryitem['stockNo'],
                            item['date'],
                            item['closePrice'],
                            item['sma5'],
                            WPCT))

                        if item['closePrice'] < item['upper']:
                            print('{0}-{1}'.format(stockName, stockNo))
                            print("buy date {0} - sell date {1}".format(hitsoryitem['buyDate'], item['date']))
                            print("buy price {0}- sell price {1}".format(hitsoryitem['buyPrice'],
                                                                         item['closePrice']))

                            obs_analysis_tabel.update(
                                {"_id": hitsoryitem['_id']},
                                {'$set': {'isSell': True, 'sellDate': item['date'],
                                          'sellPrice': item['closePrice'],
                                          'wpct': WPCT, 'cost': cost}})
                            break
                    elif WPCT < -10:
                        print('{0}-{1}'.format(stockName, stockNo))
                        print("buy date {0} - sell date {1}".format(hitsoryitem['buyDate'], item['date']))
                        print("buy price {0}- sell price {1}".format(hitsoryitem['buyPrice'],
                                                                     item['closePrice']))

                        obs_analysis_tabel.update(
                            {"_id": hitsoryitem['_id']},
                            {'$set': {'isSell': True, 'sellDate': item['date'],
                                      'sellPrice': item['closePrice'],
                                      'wpct': WPCT, 'cost': cost}})
                        break
                    else:
                        if item['closePrice'] < item['sma5']:
                            date1 = item['date'] - timedelta(days=1)
                            date2 = item['date'] - timedelta(days=2)
                            date3 = item['date'] - timedelta(days=3)
                            day1 = find_agents(stockNo, date1.strftime("%Y%m%d"), 20)
                            if float(day1['concentrated']) < 0:
                                day2 = find_agents(stockNo, date2.strftime("%Y%m%d"), 20)
                                if float(day2['concentrated']) < 0:
                                    day3 = find_agents(stockNo, date3.strftime("%Y%m%d"), 20)
                                    if float(day3['concentrated']) < 0:
                                        print('{0}-{1}'.format(stockName, stockNo))
                                        print(
                                            "buy date {0} - sell date {1}".format(hitsoryitem['buyDate'], item['date']))
                                        print("buy price {0}- sell price {1}".format(hitsoryitem['buyPrice'],
                                                                                     item['closePrice']))

                                        obs_analysis_tabel.update({"_id": hitsoryitem['_id']},
                                                                  {'$set': {'isSell': True,
                                                                            'sellDate': item['date'],
                                                                            'sellPrice': item['closePrice'],
                                                                            'wpct': WPCT, 'cost':cost}})
                                        break

                    print('update {0}-{1} Date:{2} closePrice:{3} sma5:{4} WPCT:{5}'.format(hitsoryitem['stockName'],
                                                                                            hitsoryitem['stockNo'],
                                                                                            item['date'],
                                                                                            item['closePrice'],
                                                                                            item['sma5'],
                                                                                            WPCT))
                    obs_analysis_tabel.update({"_id": hitsoryitem['_id']}, {'$set': {'wpct': WPCT}})


def chip_analysis(item, date_str, simulation=False):
    item_count = 120

    stock = item['stockNo']
    name = item['stockName']

    details = item['details']

    if len(details) == 0:
        return

    details.sort(key=itemgetter('date'), reverse=False)

    df = pd.DataFrame(list(details))
    df['Date'] = pd.to_datetime(df['date'])
    df.set_index('Date', inplace=True)
    df.sort_index(inplace=True)
    df.head()

    end = datetime.strptime(date_str + " 23:59:59", '%Y%m%d %H:%M:%S')

    mask = (df['date'] <= end)

    df = df.loc[mask][-item_count:]

    closeList = df.fillna(0).closePrice.tolist()
    if len(closeList) == 0:
        return
    if stock is None:
        return

    upper, middle, lower = talib.BBANDS(numpy.array(closeList),
                                        timeperiod=20, nbdevup=2.1,
                                        nbdevdn=2.1, matype=0)

    print('{0}{1}'.format(stock, name))
    if over_60ma(df):
        if bbandzip(df, upper, lower, 20):
            data20 = find_agents(stock, date_str, 20)
            if data20['total'] > 1:
                data60 = find_agents(stock, date_str, 60)
                if data60['total'] > 3:
                    r = collectTWSE.find_one({'stock_id': '{0}'.format(stock)})

                    item = {
                        'createDate': df['date'][-1],
                        'stockName': name,
                        'type': r['type'],
                        'stockNo': stock,
                        'closePrice': df['closePrice'][-1],
                        'openPrice': df['openPrice'][-1],
                        'highPrice': df['highPrice'][-1],
                        'lowPrice': df['lowPrice'][-1],
                        'stockChip': data20['stockChip'],
                        'total20': data20['total'],
                        'total60': data60['total'],
                        'avg20': data20['avg'],
                        'avg60': data60['avg'],
                        'concentrated20': data20['concentrated'],
                        'concentrated60': data60['concentrated'],
                        'buyDate': None,
                        'buyPrice': 0.0,
                        'sellDate': None,
                        'sellPrice': 0.0,
                        'wpct': 0.0,
                        'isSell': False,
                        'cost': 0
                    }

                    if simulation:
                        obs_analysis_tabel.insert_one(item)
                        history_analysis_chip(item)
                    else:
                        real_time.insert_one(item)

                    print("\r\n----------------------------------------------------------------------------------")
                    print("{0}-{1}".format(name, stock))

                    print('日期 {5} 收盤價 {0}, 開盤 {1}, 最高 {2}, 最低 {3}, 成交量 {4}'.format(
                        df['closePrice'][-1],
                        df['openPrice'][-1],
                        df['highPrice'][-1],
                        df['lowPrice'][-1],
                        df['dealValue'][-1] / 1000,
                        df['date'][-1].strftime("%Y-%m-%d")))
                    print("\r\n股本 : {0},買超股本 20/60 日：{1}%/{2}%, "
                          "20/60 均價：{3}/{4} 籌碼集中：{5}%/{6}%\r\n".format(
                        data20['stockChip'],
                        "%.1f" % data20['total'],
                        "%.1f" % data60['total'],
                        "%.1f" % data20['avg'],
                        "%.1f" % data60['avg'],
                        "%.1f" % data20['concentrated'],
                        "%.1f" % data60['concentrated']))


def main():
    twse = collect.find({})
    item = twse[0]
    details = item['details']
    details.sort(key=itemgetter('date'), reverse=False)

    df = pd.DataFrame(list(details))
    df['Date'] = pd.to_datetime(df['date'])
    df.set_index('Date', inplace=True)
    df.sort_index(inplace=True)
    df.head()
    end = datetime.strptime("20170801 23:59:59", '%Y%m%d %H:%M:%S')
    mask = (df['date'] > end)
    df = df.loc[mask]

    dateList = df['date'].tolist()

    ignoreList = ['4303', '2337']

    for da in dateList:
        print(da)
        t = collect.find({})
        for index, item in enumerate(t):
            try:
                if item['stockNo'] in ignoreList:
                    continue
                itm = obs_analysis_tabel.find_one({'stockNo': item['stockNo'], 'isSell': False})
                if itm is not None:
                    continue
                chip_analysis(item, da.strftime("%Y%m%d"), True)
            except Exception as e:
                print('{}-{}'.format(item['stockNo'], e))


if __name__ == '__main__':
    main()
