#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import configparser
from datetime import datetime, timedelta
from operator import attrgetter, itemgetter
from bs4 import BeautifulSoup
from pymongo import MongoClient

config = configparser.ConfigParser()
config.optionxform = str
config.read('config.ini')
host = config.get('DEFAULT', 'mongodb_host')
port = config.get('DEFAULT', 'mongodb_port')
client = MongoClient(host, int(port))
db = client['stock']
major_db = db['major']
collectTWSE = db['twse_list']

headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36"}

twse = collectTWSE.find()
row_count = twse.count()

for index, item in enumerate(twse):
    stock_no = item['stock_id']
    stock_name = item['stock_name']
    url = "http://www.wantgoo.com/stock/astock/chips?StockNo={0}".format(stock_no)
    res = requests.post(url, headers=headers)
    soup = BeautifulSoup(res.text, 'html.parser')
    print("stock no {0}".format(stock_no))
    table = soup.find('table', attrs={'class': 'tb rw5n tbhl'})

    if table is None:
        continue
    table_body = table.find('tbody')
    rows = table_body.find_all('tr')
    data = []

    json_set_array = []

    for row in rows:
        cols = row.find_all('td')
        cols = [ele.text.strip() for ele in cols]

        date = ""
        chips = ""
        fund = ""
        major = ""
        supervisor = ""
        try:
            date = cols[0]
            if date == '':
                date = datetime.today()
        except IndexError:
            date = ""

        try:
            chips = cols[1]
            if chips == '':
                chips = "0"
        except IndexError:
            chips = "0"

        try:
            fund = cols[2]
            if fund == '':
                fund = "0"
        except IndexError:
            fund = "0"

        try:
            major = cols[3]
            if major == '':
                major = "0"
        except IndexError:
            major = "0"

        try:
            supervisor = cols[4]
            if supervisor == '':
                supervisor = "0"
        except IndexError:
            supervisor = "0"

        json_set = {'date': datetime.strptime(date, '%Y/%m/%d'),
                    'chips': float(chips),
                    'fund': float(fund),
                    'major': float(major),
                    'supervisor': float(supervisor)
                    }
        json_set_array.append(json_set)

    item = major_db.find_one({"stockNo": stock_no})

    if item is None:
        stock_data = {
            'stockNo': stock_no,
            'stockName': stock_name,
            'details': json_set_array
        }
        major_db.insert_one(stock_data)
    else:
        details = item['details']
        details.sort(key=itemgetter('date'), reverse=True)
        db_date = details[0]['date'].strftime("%Y-%m-%d 00:00:00")
        friday = datetime.now() - timedelta(days=datetime.now().weekday()) + timedelta(days=7)
        friday = friday.strftime("%Y-%m-%d 00:00:00")

        if db_date != friday:
            major_db.update({"stockNo": stock_no}, {'$push': {'details': json_set_array[0]}})
        else:
            print("Data exist")


