import json
import os
from operator import itemgetter

import requests
from bs4 import BeautifulSoup
from dateutil.relativedelta import relativedelta
from pymongo import MongoClient
from datetime import datetime
client = MongoClient('127.0.0.1', int('27017'))
db = client['stock']
chip_db = db['fund']
chip_daily_db = db['chip_daily']
collectTWSE = db['twse_list']

headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36"}

twse = collectTWSE.find()
row_count = twse.count()

for index, item in enumerate(twse):
    stock_no = item['stock_id']
    stock_name = item['stock_name']
    url = "http://www.wantgoo.com/stock/astock/three?stockno={0}&dtSpan=40".format(stock_no)
    res = requests.post(url, headers=headers)
    soup = BeautifulSoup(res.text, 'html.parser')
    print("stock no {0}".format(stock_no))
    table = soup.find('table', attrs={'class': 'tb rw5n tbhl'})
    if table is None:
        continue
    table_body = table.find('tbody')
    rows = table_body.find_all('tr')
    data = []

    json_set_array = []

    for row in rows:
        cols = row.find_all('td')
        cols = [ele.text.strip() for ele in cols]

        date = ""
        foreign = ""
        investment = ""
        dealer = ""
        dailyTotal = ""
        try:
            date = cols[0]
            if date == '':
                date = datetime.today()
        except IndexError:
            date = ""

        try:
            chips = cols[1]
            if chips == '':
                chips = "0"
        except IndexError:
            chips = "0"

        try:
            fund = cols[2]
            if fund == '':
                fund = "0"
        except IndexError:
            fund = "0"

        try:
            major = cols[3]
            if major == '':
                major = "0"
        except IndexError:
            major = "0"

        try:
            supervisor = cols[4]
            if supervisor == '':
                supervisor = "0"
        except IndexError:
            supervisor = "0"
        if date == '合計買賣超' or date == '※外資自營商買賣股數已計入自營商買賣股數，故不納入三大法人買賣股數之合計數計算。':
            continue

        json_set = {'date': datetime.strptime(date, '%Y/%m/%d'),
                    'chips': float(chips.replace(',', '')),
                    'fund': float(fund.replace(',', '')),
                    'major': float(major.replace(',', '')),
                    'supervisor': float(supervisor.replace(',', ''))
                    }
        json_set_array.append(json_set)

    item = chip_db.find_one({"stockNo": stock_no})

    if item is None:
        stock_data = {
            'stockNo': stock_no,
            'stockName': stock_name,
            'details': json_set_array
        }
        chip_db.insert_one(stock_data)
    else:
        details = item['details']
        details.sort(key=itemgetter('date'), reverse=True)
        db_date = details[0]['date'].strftime("%Y-%m-%d 00:00:00")
        friday = datetime.now() - timedelta(days=datetime.now().weekday()) + timedelta(days=7)
        friday = friday.strftime("%Y-%m-%d 00:00:00")

        if db_date != friday:
            chip_db.update({"stockNo": stock_no}, {'$push': {'details': json_set_array[0]}})
        else:
            print("Data exist")
