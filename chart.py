# coding:utf-8
import uuid
from datetime import datetime, timedelta
from operator import itemgetter
import configparser

import numpy
import numpy as np
import matplotlib.pyplot as plt
import shutil
import talib
from pymongo import MongoClient
import pandas as pd
import matplotlib.dates as mdates
import matplotlib.ticker as mticker
from matplotlib.finance import candlestick_ohlc
import os
import matplotlib

matplotlib.rcParams['font.sans-serif'] = 'SimHei'
client = MongoClient('127.0.0.1', 27017)
db = client['stock']
collectStock = db['stock']

MA1 = 10
MA2 = 30


class Chart(object):
    def moving_average(self, values, window):
        weights = np.repeat(1.0, window) / window
        smas = np.convolve(values, weights, 'valid')
        return smas

    def high_minus_low(self, highs, lows):
        return highs - lows

    def bytespdate2num(self, fmt, encoding='utf-8'):
        strconverter = mdates.strpdate2num(fmt)

        def bytesconverter(b):
            s = b.decode(encoding)
            return strconverter(s)

        return bytesconverter

    def format_date(self, x, pos=None):
        thisind = np.clip(int(x + 0.5), 0, N - 1)
        return r.date[thisind].strftime('%Y-%m-%d')

    def movingaveage(self, values, window):
        weights = np.repeat(1.0, window) / window
        smas = np.convolve(values, weights, 'valid')
        return smas

    def expMovingAveage(self, values, window):
        weights = np.exp(np.linspace(-1., 0., window))
        weights /= weights.sum()
        a = np.convolve(values, weights, mode='full')[:len(values)]
        a[:window] = a[window]
        return a

    def computeMACD(self, x, slow=26, fast=12):
        emslow = self.expMovingAveage(x, slow)
        emfast = self.expMovingAveage(x, fast)
        return emslow, emfast, emfast - emslow

    def getData(self, stockNo):
        ma1 = 10
        ma2 = 20

        start = datetime.now() + timedelta(-120)
        end = datetime.today()
        result = collectStock.find_one({'stockNo': stockNo})
        details = result['details']
        stockName = result['stockName']

        details.sort(key=itemgetter('date'), reverse=False)

        df = pd.DataFrame(list(details))
        df = df.set_index(['date'])
        df['Date'] = df.index


        mask = (df['Date'] > start) & (df['Date'] <= end)
        df = df.loc[mask]

        df['Date'] = df['Date'].map(mdates.date2num)

        for ix in df.index:
            row = df.loc[ix]
            df.loc[ix, 'dealValue'] = row.dealValue / 1000



        date = df['Date']
        closep = df['closePrice']
        highp = df['highPrice']
        lowp = df['lowPrice']
        openp = df['openPrice']
        volume = df['dealValue']

        fig = plt.figure(facecolor='#07000d')

        title = '{0}-{1}\n收盤:{2} 最高:{3} 最低:{4} 開盤:{5}'.format(
            stockName,
            stockNo,
            closep[-1],
            highp[-1], lowp[-1], openp[-1])

        fig.suptitle(title, color='w', fontsize=20, x=0.5)

        ax1 = plt.subplot2grid((5, 4), (0, 0), rowspan=4, colspan=4, axisbg='#07000d')
        ax1.yaxis.label.set_color('w')
        ax1.spines['bottom'].set_color('#5998ff')
        ax1.spines['top'].set_color('#5998ff')
        ax1.spines['left'].set_color('#5998ff')
        ax1.spines['right'].set_color('#5998ff')
        ax1.grid(b=True, which='major', color='w', linestyle='--', linewidth=0.5, alpha=0.2)
        ax1.tick_params(axis='y', colors='w', labelsize=10)
        ax1.tick_params(axis='x', colors='w', labelsize=10)
        ax1.xaxis.set_major_locator(mticker.MaxNLocator(30))
        x = 0
        y = len(df['Date'].values)
        ohlc = []

        while x < y:
            append_me = date[x], openp[x], highp[x], lowp[x], closep[x], volume[x] / 1000
            ohlc.append(append_me)
            x += 1

        ohlc_data_arr = np.array(ohlc)
        ohlc_data_arr2 = np.hstack(
            [np.arange(ohlc_data_arr[:, 0].size)[:, np.newaxis], ohlc_data_arr[:, 1:]])

        ndays = ohlc_data_arr2[:, 0]

        av1 = self.moving_average(closep, ma1)
        av2 = self.moving_average(closep, ma2)
        sp1 = len(ndays[ma2 - 1:])
        label1 = str(ma1) + ' SMA'
        label2 = str(ma2) + ' SMA'
        ax1.plot(ndays[-sp1:], av1[-sp1:], lw=0.5, label=label1)
        ax1.plot(ndays[-sp1:], av2[-sp1:], lw=0.5, label=label2)

        candlestick_ohlc(ax1, ohlc_data_arr2, width=0.4, colorup='#db3f3f', colordown='#77d879')
        plt.setp(plt.gca().get_xticklabels(), horizontalalignment='center')

        dd = []
        for d in df.index:
            dd.append(d.strftime("%m/%d"))

        diff = (highp.max() - lowp.min())
        ax1.set_ylim(lowp.min() - diff, highp.max() + (diff / 2))
        ax1.grid(True)
        ax1.set_xticks(ndays[::5])
        ax1.set_xticklabels(dd[::5])
        ax1.set_xlim(ndays.min(), ndays.max())
        plt.ylabel('Stock Price')
        plt.legend(loc=9, ncol=3, prop={'size': 6}, fancybox=True, borderaxespad=0.)

        ax1v = ax1.twinx()
        ax1v.bar(ndays, volume, width=0.4, color='#00ffe8', alpha=.2, align='edge', label='volume')
        ax1v.grid(False)
        ax1v.set_ylim(0, 2 * volume.max())
        ax1v.yaxis.label.set_color('w')
        ax1v.spines['bottom'].set_color('#5998ff')
        ax1v.spines['top'].set_color('#5998ff')
        ax1v.spines['left'].set_color('#5998ff')
        ax1v.spines['right'].set_color('#5998ff')
        ax1v.tick_params(axis='y', colors='w', labelsize=10)
        ax1v.tick_params(axis='x', colors='w', labelsize=10)

        ax2 = plt.subplot2grid((6, 4), (5, 0), sharex=ax1, rowspan=1, colspan=4, axisbg='#07000d')

        slowk, slowd = talib.STOCH(numpy.array(highp.tolist()),
                                   numpy.array(lowp.tolist()),
                                   numpy.array(closep.tolist()),
                                   fastk_period=9,
                                   slowk_period=3,
                                   slowk_matype=0,
                                   slowd_period=3,
                                   slowd_matype=0)
        ax2.plot(ndays, slowk, color='b', lw=0.5)
        ax2.plot(ndays, slowd, color='r', lw=0.5)

        """
        emslow, emdast, macd = self.computeMACD(closep)
        ema9 = self.expMovingAveage(macd, 9)
        ax2.plot(ndays, macd, color='b', lw=0.5)
        ax2.plot(ndays, ema9, color='r', lw=0.5)
        ax2.bar(ndays, macd - ema9, width=0.4, facecolor='#00ffe8', alpha=.2, align='edge', label='volume')

        """

        ax2.spines['bottom'].set_color('#5998ff')
        ax2.spines['top'].set_color('#5998ff')
        ax2.spines['left'].set_color('#5998ff')
        ax2.spines['right'].set_color('#5998ff')
        ax2.tick_params(axis='y', colors='w')
        ax2.tick_params(axis='x', colors='w', labelsize=10)
        plt.ylabel('KD', color='w')

        # plt.show()
        fig.set_size_inches(9.2, 9)

        path = 'static/{0}/{1}'.format(stockNo, datetime.today().strftime("%Y%m%d"))

        if not os.path.exists(path):
            os.makedirs(path)
        else:
            shutil.rmtree('static/{0}'.format(stockNo))
            os.makedirs(path)

        fig.savefig(path + '/stock.png', facecolor=fig.get_facecolor(), dpi=100)

        # fig.savefig(imgPath240, facecolor=fig.get_facecolor(), dpi=100)
        # imgPath240 = '{0}/240.png'.format(path)
        # fig.savefig(imgPath240, facecolor=fig.get_facecolor(), dpi=100)
        # imgPath300 = '{0}/300.png'.format(path)
        # fig.savefig(imgPath300, facecolor=fig.get_facecolor(), dpi=100)
        # imgPath460 = '{0}/460.png'.format(path)
        # fig.savefig(imgPath460, facecolor=fig.get_facecolor(), dpi=100)

        # imgPath700 = '{0}/700.png'.format(path)
        # fig.savefig(imgPath700, facecolor=fig.get_facecolor(), dpi=100)

        # imgPath1040 = '{0}/1040.png'.format(path)
        # fig.savefig(imgPath1040, facecolor=fig.get_facecolor(), dpi=100)

        plt.close(fig)

        # os.rename(imgPath240, imgPath240.replace('.png', ''))
        # os.rename(imgPath300, imgPath300.replace('.png', ''))
        # os.rename(imgPath460, imgPath460.replace('.png', ''))
        # os.rename(imgPath700, imgPath700.replace('.png', ''))
        # os.rename(imgPath1040, imgPath1040.replace('.png', ''))

if __name__ == '__main__':
    c = Chart()
    c.getData('3443')
